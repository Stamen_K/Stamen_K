CREATE DATABASE  IF NOT EXISTS `match_score` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `match_score`;
-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: match_score
-- ------------------------------------------------------
-- Server version	5.5.5-10.9.2-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=451 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (253,'Afghanistan'),(254,'Albania'),(255,'Algeria'),(256,'Andorra'),(257,'Angola'),(258,'Anguilla'),(259,'Antigua & Barbuda'),(260,'Argentina'),(261,'Armenia'),(262,'Australia'),(263,'Austria'),(264,'Azerbaijan'),(265,'Bahamas'),(266,'Bahrain'),(267,'Bangladesh'),(268,'Barbados'),(269,'Belarus'),(270,'Belgium'),(271,'Belize'),(272,'Benin'),(273,'Bermuda'),(274,'Bhutan'),(275,'Bolivia'),(276,'Bosnia & Herzegovina'),(277,'Botswana'),(278,'Brazil'),(279,'Brunei Darussalam'),(280,'Bulgaria'),(281,'Burkina Faso'),(282,'Myanmar/Burma'),(283,'Burundi'),(284,'Cambodia'),(285,'Cameroon'),(286,'Canada'),(287,'Cape Verde'),(288,'Cayman Islands'),(289,'Central African Republic'),(290,'Chad'),(291,'Chile'),(292,'China'),(293,'Colombia'),(294,'Comoros'),(295,'Congo'),(296,'Costa Rica'),(297,'Croatia'),(298,'Cuba'),(299,'Cyprus'),(300,'Czech Republic'),(301,'Democratic Republic of the Congo'),(302,'Denmark'),(303,'Djibouti'),(304,'Dominican Republic'),(305,'Dominica'),(306,'Ecuador'),(307,'Egypt'),(308,'El Salvador'),(309,'Equatorial Guinea'),(310,'Eritrea'),(311,'Estonia'),(312,'Ethiopia'),(313,'Fiji'),(314,'Finland'),(315,'France'),(316,'French Guiana'),(317,'Gabon'),(318,'Gambia'),(319,'Georgia'),(320,'Germany'),(321,'Ghana'),(322,'Great Britain'),(323,'Greece'),(324,'Grenada'),(325,'Guadeloupe'),(326,'Guatemala'),(327,'Guinea'),(328,'Guinea-Bissau'),(329,'Guyana'),(330,'Haiti'),(331,'Honduras'),(332,'Hungary'),(333,'Iceland'),(334,'India'),(335,'Indonesia'),(336,'Iran'),(337,'Iraq'),(338,'Israel and the Occupied Territories'),(339,'Italy'),(340,'Ivory Coast (Cote d\'Ivoire)'),(341,'Jamaica'),(342,'Japan'),(343,'Jordan'),(344,'Kazakhstan'),(345,'Kenya'),(346,'Kosovo'),(347,'Kuwait'),(348,'Kyrgyz Republic (Kyrgyzstan)'),(349,'Laos'),(350,'Latvia'),(351,'Lebanon'),(352,'Lesotho'),(353,'Liberia'),(354,'Libya'),(355,'Liechtenstein'),(356,'Lithuania'),(357,'Luxembourg'),(358,'Republic of Macedonia'),(359,'Madagascar'),(360,'Malawi'),(361,'Malaysia'),(362,'Maldives'),(363,'Mali'),(364,'Malta'),(365,'Martinique'),(366,'Mauritania'),(367,'Mauritius'),(368,'Mayotte'),(369,'Mexico'),(370,'Moldova, Republic of'),(371,'Monaco'),(372,'Mongolia'),(373,'Montenegro'),(374,'Montserrat'),(375,'Morocco'),(376,'Mozambique'),(377,'Namibia'),(378,'Nepal'),(379,'Netherlands'),(380,'New Zealand'),(381,'Nicaragua'),(382,'Niger'),(383,'Nigeria'),(384,'Korea, Democratic Republic of (North Korea)'),(385,'Norway'),(386,'Oman'),(387,'Pacific Islands'),(388,'Pakistan'),(389,'Panama'),(390,'Papua New Guinea'),(391,'Paraguay'),(392,'Peru'),(393,'Philippines'),(394,'Poland'),(395,'Portugal'),(396,'Puerto Rico'),(397,'Qatar'),(398,'Reunion'),(399,'Romania'),(400,'Russian Federation'),(401,'Rwanda'),(402,'Saint Kitts and Nevis'),(403,'Saint Lucia'),(404,'Saint Vincent\'s & Grenadines'),(405,'Samoa'),(406,'Sao Tome and Principe'),(407,'Saudi Arabia'),(408,'Senegal'),(409,'Serbia'),(410,'Seychelles'),(411,'Sierra Leone'),(412,'Singapore'),(413,'Slovak Republic (Slovakia)'),(414,'Slovenia'),(415,'Solomon Islands'),(416,'Somalia'),(417,'South Africa'),(418,'Korea, Republic of (South Korea)'),(419,'South Sudan'),(420,'Spain'),(421,'Sri Lanka'),(422,'Sudan'),(423,'Suriname'),(424,'Swaziland'),(425,'Sweden'),(426,'Switzerland'),(427,'Syria'),(428,'Tajikistan'),(429,'Tanzania'),(430,'Thailand'),(431,'Timor Leste'),(432,'Togo'),(433,'Trinidad & Tobago'),(434,'Tunisia'),(435,'Turkey'),(436,'Turkmenistan'),(437,'Turks & Caicos Islands'),(438,'Uganda'),(439,'Ukraine'),(440,'United Arab Emirates'),(441,'United States of America (USA)'),(442,'Uruguay'),(443,'Uzbekistan'),(444,'Venezuela'),(445,'Vietnam'),(446,'Virgin Islands (UK)'),(447,'Virgin Islands (US)'),(448,'Yemen'),(449,'Zambia'),(450,'Zimbabwe');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `link_requests`
--

DROP TABLE IF EXISTS `link_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `link_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `player_id` int(11) DEFAULT NULL,
  `sent_at` date NOT NULL,
  `status` varchar(45) NOT NULL,
  `admin_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_link_requests_user1_idx` (`user_id`),
  KEY `fk_link_requests_player1_idx` (`player_id`),
  KEY `fk_link_requests_user2_idx` (`admin_id`),
  CONSTRAINT `fk_link_requests_player1` FOREIGN KEY (`player_id`) REFERENCES `player` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_link_requests_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_link_requests_user2` FOREIGN KEY (`admin_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `link_requests`
--

LOCK TABLES `link_requests` WRITE;
/*!40000 ALTER TABLE `link_requests` DISABLE KEYS */;
INSERT INTO `link_requests` VALUES (6,13,89,'2022-11-16','accepted',12);
/*!40000 ALTER TABLE `link_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `match`
--

DROP TABLE IF EXISTS `match`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `match` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `played_at` date NOT NULL,
  `match_format_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_match_match_format1_idx` (`match_format_id`),
  CONSTRAINT `fk_match_match_format1` FOREIGN KEY (`match_format_id`) REFERENCES `match_format` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `match`
--

LOCK TABLES `match` WRITE;
/*!40000 ALTER TABLE `match` DISABLE KEYS */;
INSERT INTO `match` VALUES (4,'Grand prix','2022-12-17',2),(5,'Grand prix rematch','2022-12-19',2),(7,'Friendly Football','2022-12-20',3),(8,'Grand prix Sofia','2022-12-21',2),(10,'Grand prix Belgrade','2022-12-14',2);
/*!40000 ALTER TABLE `match` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `match_format`
--

DROP TABLE IF EXISTS `match_format`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `match_format` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `rules` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `match_format`
--

LOCK TABLES `match_format` WRITE;
/*!40000 ALTER TABLE `match_format` DISABLE KEYS */;
INSERT INTO `match_format` VALUES (2,'Player match','Best player wins'),(3,'Team match','Best team wins');
/*!40000 ALTER TABLE `match_format` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matchups`
--

DROP TABLE IF EXISTS `matchups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `matchups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tournament_id` int(11) NOT NULL,
  `played_at` date NOT NULL,
  `tournament_phase` int(11) NOT NULL,
  `player_one` int(11) DEFAULT NULL,
  `player_two` int(11) DEFAULT NULL,
  `player_one_score` int(11) DEFAULT NULL,
  `player_two_score` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_round_player1_idx` (`player_one`),
  KEY `fk_round_player2_idx` (`player_two`),
  KEY `fk_game_tournament1_idx` (`tournament_id`),
  CONSTRAINT `fk_game_tournament1` FOREIGN KEY (`tournament_id`) REFERENCES `tournament` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_round_player1` FOREIGN KEY (`player_one`) REFERENCES `player` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_round_player2` FOREIGN KEY (`player_two`) REFERENCES `player` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=314 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matchups`
--

LOCK TABLES `matchups` WRITE;
/*!40000 ALTER TABLE `matchups` DISABLE KEYS */;
INSERT INTO `matchups` VALUES (305,33,'2024-12-22',1,94,90,NULL,NULL),(306,33,'2024-12-22',1,89,93,NULL,NULL),(307,33,'2024-12-22',2,NULL,NULL,NULL,NULL),(308,34,'2024-12-19',2,90,94,NULL,NULL),(309,34,'2024-12-19',2,89,93,NULL,NULL),(310,34,'2024-12-26',3,90,93,NULL,NULL),(311,34,'2024-12-26',3,94,89,NULL,NULL),(312,34,'2024-12-12',1,90,89,NULL,NULL),(313,34,'2024-12-12',1,93,94,NULL,NULL);
/*!40000 ALTER TABLE `matchups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player`
--

DROP TABLE IF EXISTS `player`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `player` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `second_name` varchar(45) NOT NULL,
  `team_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_player_team1_idx` (`team_id`),
  KEY `fk_player_country1_idx` (`country_id`),
  CONSTRAINT `fk_player_country1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_player_team1` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player`
--

LOCK TABLES `player` WRITE;
/*!40000 ALTER TABLE `player` DISABLE KEYS */;
INSERT INTO `player` VALUES (89,'Fernando','Alonso',NULL,NULL),(90,'Lewis','Hamilton',NULL,NULL),(93,'Max','Verstappen',NULL,NULL),(94,'Sebastian','Vettel',NULL,NULL);
/*!40000 ALTER TABLE `player` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player_match_detail`
--

DROP TABLE IF EXISTS `player_match_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `player_match_detail` (
  `player_id` int(11) NOT NULL,
  `match_id` int(11) NOT NULL,
  `score` int(11) DEFAULT NULL,
  PRIMARY KEY (`player_id`,`match_id`),
  KEY `fk_match_detail_match1_idx` (`match_id`),
  CONSTRAINT `fk_match_detail_match1` FOREIGN KEY (`match_id`) REFERENCES `match` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_match_detail_player` FOREIGN KEY (`player_id`) REFERENCES `player` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player_match_detail`
--

LOCK TABLES `player_match_detail` WRITE;
/*!40000 ALTER TABLE `player_match_detail` DISABLE KEYS */;
INSERT INTO `player_match_detail` VALUES (89,4,NULL),(89,5,NULL),(89,8,NULL),(89,10,NULL),(90,4,NULL),(90,5,NULL),(93,8,NULL),(94,10,NULL);
/*!40000 ALTER TABLE `player_match_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `promotion_requests`
--

DROP TABLE IF EXISTS `promotion_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `promotion_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `sent_at` date NOT NULL,
  `status` varchar(45) NOT NULL,
  `admin_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_promotion_requests_user1_idx` (`user_id`),
  KEY `fk_promotion_requests_user2_idx` (`admin_id`),
  CONSTRAINT `fk_promotion_requests_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_promotion_requests_user2` FOREIGN KEY (`admin_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promotion_requests`
--

LOCK TABLES `promotion_requests` WRITE;
/*!40000 ALTER TABLE `promotion_requests` DISABLE KEYS */;
/*!40000 ALTER TABLE `promotion_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team`
--

DROP TABLE IF EXISTS `team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team`
--

LOCK TABLES `team` WRITE;
/*!40000 ALTER TABLE `team` DISABLE KEYS */;
INSERT INTO `team` VALUES (12,'Best team'),(13,'Worst team');
/*!40000 ALTER TABLE `team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_match_detail`
--

DROP TABLE IF EXISTS `team_match_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `team_match_detail` (
  `match_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `score` int(11) DEFAULT NULL,
  PRIMARY KEY (`match_id`,`team_id`),
  KEY `fk_match_detail_match1_idx` (`match_id`),
  KEY `fk_match_detail_copy1_team1_idx` (`team_id`),
  CONSTRAINT `fk_match_detail_copy1_team1` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_match_detail_match10` FOREIGN KEY (`match_id`) REFERENCES `match` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team_match_detail`
--

LOCK TABLES `team_match_detail` WRITE;
/*!40000 ALTER TABLE `team_match_detail` DISABLE KEYS */;
INSERT INTO `team_match_detail` VALUES (7,12,0),(7,13,2);
/*!40000 ALTER TABLE `team_match_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tournament`
--

DROP TABLE IF EXISTS `tournament`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tournament` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `prize` varchar(45) NOT NULL,
  `tournament_format_id` int(11) NOT NULL,
  `winner` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tournament_tournament_format1_idx` (`tournament_format_id`),
  KEY `fk_tournament_player1_idx` (`winner`),
  CONSTRAINT `fk_tournament_player1` FOREIGN KEY (`winner`) REFERENCES `player` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tournament_tournament_format1` FOREIGN KEY (`tournament_format_id`) REFERENCES `tournament_format` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tournament`
--

LOCK TABLES `tournament` WRITE;
/*!40000 ALTER TABLE `tournament` DISABLE KEYS */;
INSERT INTO `tournament` VALUES (33,'Grand prix Istanbul','1000000 lv',1,NULL),(34,'First League','First league cup',2,NULL);
/*!40000 ALTER TABLE `tournament` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tournament_format`
--

DROP TABLE IF EXISTS `tournament_format`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tournament_format` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `rules` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tournament_format`
--

LOCK TABLES `tournament_format` WRITE;
/*!40000 ALTER TABLE `tournament_format` DISABLE KEYS */;
INSERT INTO `tournament_format` VALUES (1,'Knockout','Elimination bracket rules'),(2,'League','Standard league rules');
/*!40000 ALTER TABLE `tournament_format` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `password` varchar(100) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `role` varchar(45) NOT NULL,
  `associated_player` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_user_player1_idx` (`associated_player`),
  CONSTRAINT `fk_user_player1` FOREIGN KEY (`associated_player`) REFERENCES `player` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (12,'matchscore.team3@gmail.com','62ebe13847292440bbe662ac19109e29ed5c62076a82acdd6c240485dcbff015','admin',NULL),(13,'stamen_konarchev1@abv.bg','4325f02bad4052ce441307bd092dbc91f4145bb0aa9bb64e8e97a13a0a2032ec','user',89);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-11-16 14:58:16
