from fastapi import FastAPI
from routers.player import player_router
from routers.team import team_router
from routers.user import user_router
from routers.requests_router import requests_router
from routers.match import match_router
from routers.match_format import match_format_router
from routers.tournaments import tournaments_router


app = FastAPI()

app.include_router(player_router)
app.include_router(team_router)
app.include_router(user_router)
app.include_router(requests_router)
app.include_router(match_router)
app.include_router(match_format_router)
app.include_router(user_router)
app.include_router(tournaments_router)