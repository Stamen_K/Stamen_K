from fastapi import APIRouter, Header
from services import team_service, player_service, user_service
from data.models import Team
from common.responses import BadRequest, NotFound, Successful, Unauthorized


team_router = APIRouter(prefix='/team')


@team_router.post('/')
def add_team(team: Team, x_token: str = Header()):
    user_auth = user_service.from_token(x_token)
    if not user_auth: return Unauthorized("Wrong or expired token")

    user = user_service.find_user(user_auth)
    if not user.is_admin() and not user.is_director(): return Unauthorized("Only directors and admins can create teams")
    
    result = team_service.create_team(team)

    if not result: return BadRequest("Team name already taken")
    return result


@team_router.get('/')
def all_teams():
    return team_service.all_team()


@team_router.get('/{id}')
def get_team(id: int):
    team = team_service.get_team_by_id(id)
    if not team: return NotFound("Team does not exist")

    return team


@team_router.delete('/{id}')
def delete_team(id: int, x_token: str = Header()):
    user_auth = user_service.from_token(x_token)
    if not user_auth: return Unauthorized("Wrong or expired token")

    user = user_service.find_user(user_auth)
    if not user.is_admin() and not user.is_director(): return Unauthorized("Only directors and admins can delete teams")
    
    team = team_service.get_team_by_id(id)
    if not team: return NotFound("Team does not exist")
    
    player_service.null_team(id)
    team_service.delete_team(id)

    return Successful(f"Successfully deleted team {id} and updated players")