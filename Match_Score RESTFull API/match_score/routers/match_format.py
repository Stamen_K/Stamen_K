from fastapi import APIRouter
from services import match_format_service
from data.models import MatchFormat, Sort
from common.responses import NotFound


match_format_router = APIRouter(prefix='/match_format')


@match_format_router.get('/')
def all_formats(sort: Sort | None = None, sort_by: str | None = None, search: str | None = None):
    formats = list(match_format_service.all_formats(search))

    if sort:
        return match_format_service.sort(formats, reverse=sort == 'desc', attribute=sort_by)
    else:
        return formats


@match_format_router.get('/{id}')
def get_format_by_id(id: int):
    format = match_format_service.get_by_id(id)

    if not format: 
        return NotFound('No such format')

    return format


@match_format_router.post('/')
def create_format(format: MatchFormat):    
    created_format = match_format_service.create(format)

    return created_format


@match_format_router.put('/{id}')
def update_format(id: int, data: MatchFormat):    
    format = match_format_service.get_by_id(id)
    
    if not format: return NotFound('No such format')

    format = match_format_service.update(data, format)
    
    return format