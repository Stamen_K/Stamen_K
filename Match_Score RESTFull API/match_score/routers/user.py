from fastapi import APIRouter, Header
from data.models import LogInfo, User
from services import user_service
from common.responses import BadRequest, NotFound, Successful, Unauthorized


user_router = APIRouter(prefix='/user')


@user_router.post('/register')
def register(user: LogInfo):
    user = user_service.create_user(user)

    if not user: return BadRequest("Email already registered")

    return Successful("You have registered successfully")


@user_router.get('/login')
def login(loginfo: LogInfo):
    user = user_service.find_user(loginfo)
    
    if user: return {"token": user_service.create_token(user)} 
    return Unauthorized("Wrong login credentials")


@user_router.get('/linked_players')
def view_linked_player(x_token: str = Header()):
    user_auth = user_service.from_token(x_token)
    if not user_auth: return Unauthorized("Wrong or expired token")

    user = user_service.find_user(user_auth)
    if not user.is_admin(): return Unauthorized("Only admins can access this information")

    return user_service.select_users_with_linked_players()


@user_router.put('/null_associated_player/{id}')
def null_player(id: int, x_token: str = Header()):
    user_auth = user_service.from_token(x_token)
    if not user_auth: return Unauthorized("Wrong or expired token")

    user = user_service.find_user(user_auth)
    if not user.is_admin(): return Unauthorized("Only admins can edit this information")

    linked_user = user_service.get_user_by_id(id)
    if not linked_user: return NotFound("User not found")

    user_service.null_associated_player(linked_user)

    return Successful(f"You have removed the associated player from {linked_user.email}")

