from unittest import TestCase
from unittest.mock import patch
from services import player_service
from data.models import Player


class PlayerServiceShould(TestCase):

    
    @patch("services.player_service.database")
    def test_getTournamentPlayers_returnsCorrectly(self, mock_base):
        mock_base.read_query.return_value = [(12, 'Corry', 'Chase', 'Hungary', 'CSKA', 13, 'Peter', 'Kirilov', 'Bulgaria', 'CSKA'), 
        (78, 'John', 'Wick', 'Armenia', None, 10, 'Floyd', 'Mayweather', 'Mexico', 'Arsenal'), 
        (12, 'Corry', 'Chase', 'Hungary', 'CSKA', 10, 'Floyd', 'Mayweather', 'Mexico', 'Arsenal'), (13, 'Peter', 'Kirilov', 'Bulgaria', 'CSKA', 78, 'John', 'Wick', 'Armenia', None), 
        (12, 'Corry', 'Chase', 'Hungary', 'CSKA', 78, 'John', 'Wick', 'Armenia', None), (10, 'Floyd', 'Mayweather', 'Mexico', 'Arsenal', 13, 'Peter', 'Kirilov', 'Bulgaria', 'CSKA')]

        result = player_service.get_tournament_players(1)
        expected = [Player(id=12,first_name="Corry",second_name= "Chase",country="Hungary", team ="CSKA"),
        Player(id=13,first_name="Peter",second_name="Kirilov",country="Bulgaria",team="CSKA"),
        Player(id=78,first_name="John",second_name="Wick",country="Armenia",team=None),
        Player(id=10,first_name="Floyd",second_name="Mayweather",country="Mexico",team="Arsenal")]

        self.assertEqual(result, expected)
    

    @patch("services.player_service.database")
    def test_getTournamentPlayers_returnsCorrectlyWhenNoPlayers(self, mock_base):
        mock_base.read_query.return_value = []

        result = player_service.get_tournament_players(1)
        expected = []
        self.assertEqual(result, expected)
    
    
    @patch("services.player_service.database")
    def test_getPlayerByName_returnsCorrectly(self, mock_base):
        mock_base.read_query.return_value = [(12, 'Corry', 'Chase', 'Hungary', 'CSKA', 13, 'Peter', 'Kirilov', 'Bulgaria', 'CSKA')]

        result = player_service.get_player_by_name("Cory Chase")
        expected = Player(id=12,first_name="Corry",second_name= "Chase",country="Hungary", team ="CSKA")

        self.assertEqual(result, expected)
    

    @patch("services.player_service.database")
    def test_getPlayerByName_returnsCorrectlywhenNoPlayer(self, mock_base):
        mock_base.read_query.return_value = []

        result = player_service.get_player_by_name("Cory Chase")
        expected = None

        self.assertEqual(result, expected)
    

    @patch("services.player_service.database")
    def test_allPlayers_returnsCorrectly(self, mock_base):
        mock_base.read_query.return_value = [(12, 'Corry', 'Chase', 'Hungary', 'CSKA'),(78, 'John', 'Wick', 'Armenia', None), 
        (10, 'Floyd', 'Mayweather', 'Mexico', 'Arsenal'),(13, 'Peter', 'Kirilov', 'Bulgaria', 'CSKA')]

        result = player_service.all_players()
        expected = [Player(id=12,first_name="Corry",second_name= "Chase",country="Hungary", team ="CSKA"),
        Player(id=78,first_name="John",second_name="Wick",country="Armenia",team=None),
        Player(id=10,first_name="Floyd",second_name="Mayweather",country="Mexico",team="Arsenal"),
        Player(id=13,first_name="Peter",second_name="Kirilov",country="Bulgaria",team="CSKA")]

        self.assertEqual(result, expected)
    

    @patch("services.player_service.database")
    def test_allPlayers_returnsCorrectlyWhenNoplayers(self, mock_base):
        mock_base.read_query.return_value = []

        result = player_service.all_players()
        expected = []

        self.assertEqual(result, expected)
    
    
    @patch("services.player_service.all_players")
    @patch("services.player_service.create_player_by_name")
    def test_createUnknownParticipantsProfile_retursnCorrectly(self, mock_create, mock_all):
        mock_create.return_value = None
        mock_all.return_value = [Player(id=12,first_name="Corry",second_name= "Chase",country="Hungary", team ="CSKA"),
        Player(id=78,first_name="John",second_name="Wick",country="Armenia",team=None),
        Player(id=10,first_name="Floyd",second_name="Mayweather",country="Mexico",team="Arsenal"),
        Player(id=13,first_name="Peter",second_name="Kirilov",country="Bulgaria",team="CSKA")]

        result = player_service.create_unknown_participants_profile(["Corry Chase", "Thrud Griffon", "Baba Yaga", "John Wick"])
        expected = [["Corry", "Chase"], ["Thrud", "Griffon"], ["Baba", "Yaga"], ["John", "Wick"]]

        self.assertEqual(result, expected)
    

    @patch("services.player_service.all_players")
    @patch("services.player_service.create_player_by_name")
    def test_createUnknownParticipantsProfile_retursnCorrectlyWhenNoplayers(self, mock_create, mock_all):
        mock_create.return_value = None
        mock_all.return_value = []

        result = player_service.create_unknown_participants_profile(["Corry Chase", "Thrud Griffon", "Baba Yaga", "John Wick"])
        expected = [["Corry", "Chase"], ["Thrud", "Griffon"], ["Baba", "Yaga"], ["John", "Wick"]]

        self.assertEqual(result, expected)
    

    @patch("services.player_service.database")
    def test_countryNames_returnsCorrectly(self, mock_database):
        mock_database.read_query.return_value = [("Bulgaria",), ("Mexico",)]

        result = player_service.country_names()
        expected = ["Bulgaria", "Mexico"]

        self.assertEqual(result, expected)


    @patch("services.player_service.country_names")
    @patch("services.player_service.team_service")
    @patch("services.player_service.database")
    def test_createPlayer_returnsCorrectly(self, mock_database, mock_team, mock_country):
        mock_database.insert_query.return_value = 13
        mock_team.team_names.return_value = ["CSKA", "Arsenal"]
        mock_team.team_id.return_value = 1
        mock_country.return_value = ["Bulgaria", "Mexico"]

        result = player_service.create_player(Player(first_name="Peter",second_name="Kirilov",country="Bulgaria",team="CSKA"))
        expected = Player(id=13,first_name="Peter",second_name="Kirilov",country="Bulgaria",team="CSKA")

        self.assertEqual(result, expected)
    

    @patch("services.player_service.country_names")
    @patch("services.player_service.team_service")
    def test_createPlayer_returnsCorrectlyWhenNoSuchTeam(self, mock_team, mock_country):
        mock_team.team_names.return_value = ["Barcelona", "Arsenal"]
        mock_country.return_value = ["Bulgaria", "Mexico"]

        result = player_service.create_player(Player(first_name="Peter",second_name="Kirilov",country="Bulgaria",team="CSKA"))
        expected = None

        self.assertEqual(result, expected)
    

    @patch("services.player_service.country_names")
    def test_createPlayer_returnsCorrectlyWhenNoSuchCountry(self, mock_country):
        mock_country.return_value = ["Armenia", "Mexico"]

        result = player_service.create_player(Player(first_name="Peter",second_name="Kirilov",country="Bulgaria",team="CSKA"))
        expected = None

        self.assertEqual(result, expected)