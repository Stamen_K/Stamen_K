from datetime import datetime, timedelta
from data.models import LogInfo, Player, User, LinkedUserDetails
from data import database
import jwt
from hashlib import blake2s
from data.secrets import SALT, SECRET_KEY, ALGO
from mariadb import IntegrityError


def _hash(password: str):
    password += SALT
    return blake2s(password.encode()).hexdigest()


def create_token(user: User):
    EXPIRATION = datetime.utcnow() + timedelta(minutes=60)
    payload = {"email":user.email, "password":user.password, "exp":EXPIRATION}
    
    token = jwt.encode(payload, SECRET_KEY, algorithm=ALGO)

    return token


def from_token(token: str):
    try:
        decoded = jwt.decode(token, SECRET_KEY, algorithms=ALGO)
    except jwt.exceptions.ExpiredSignatureError: return 
    except jwt.exceptions.InvalidSignatureError: return
    except jwt.exceptions.DecodeError: return

    return LogInfo(email=decoded["email"], password=decoded["password"])


def create_user(loginfo: LogInfo):
    default_role = "user"
    h_password = _hash(loginfo.password)

    try:
        generated_id = database.insert_query("INSERT into user (email, password, role, associated_player) values(?,?,?,?)", 
        (loginfo.email, h_password, default_role, None))
    except IntegrityError: 
        return None

    return User.from_query_result(generated_id, loginfo.email, loginfo.password, default_role, None)


def find_user(loginfo: LogInfo):
    h_password = _hash(loginfo.password)

    user = database.read_query("SELECT * from user where email = ? and password = ?", (loginfo.email, h_password))
    if not user: return None
    user = user[0]

    return User.from_query_result(user[0], user[1], loginfo.password, user[3], user[4])


def get_user_by_id(id: int):
    data = database.read_query("SELECT * from user where id = ?", (id,))
    user = data[0]
    if not user: return None

    return User.from_query_result(user[0], user[1], user[2], user[3], user[4])


def make_director(user: User):
    director_role = "director"
    database.update_query('UPDATE user set role = ? where id = ?', (director_role, user.id))


def link_profile(user: User, player: Player):
    player_id = player.id
    database.update_query("UPDATE user set associated_player = ? where id = ?", (player_id, user.id))


def null_associated_player(user: User):
    database.update_query("UPDATE user set associated_player = ? where id = ?", (None, user.id))


def select_users_with_linked_players() -> list[LinkedUserDetails]: 
    data = database.read_query("""SELECT user.id, email, associated_player, first_name, second_name, team.name as team, country.name as country from user
    JOIN player on associated_player = player.id LEFT JOIN team on player.team_id = team.id LEFT JOIN country on player.country_id = country.id where associated_player is not NULL""")
    
    if not data: return []
    
    result = []
    for d in data:
        player = Player.from_query_result(d[2], d[3], d[4], d[5], d[6])
        result.append(LinkedUserDetails(user_id=d[0], user_email=d[1], player=player))
    
    return result




