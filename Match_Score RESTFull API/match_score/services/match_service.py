from data.database import read_query, insert_query, update_query
from data.models import LinkedUserDetails, Match, PlayerMatchDetailUpdate, PlayerMatchRequest, TeamMatchDetailUpdate, TeamMatchInfo,TeamMatchRequest, MatchUpdate, PlayerMatchInfo, TeamMatch, PlayerMatch
from mariadb import IntegrityError
from services import player_service, team_service, user_service
from common.responses import BadRequest, Unauthorized, Successful
from mailjet_rest import Client

def all_player_matches(search: str = None):  
    if search is None:
        data = read_query(
                '''SELECT m.id as match_id, title, played_at, match_format_id, p.id as player_id, concat(p.first_name," ", p.second_name) as player_name
                          FROM match_score.match AS m 
                            LEFT JOIN match_score.player_match_detail AS pmd ON m.id = pmd.match_id
                              JOIN match_score.player as p ON p.id = pmd.player_id''')

    else:
        data = read_query(
                '''SELECT m.id as match_id, title, played_at, match_format_id, p.id as player_id, concat(p.first_name," ", p.second_name) as player_name
                          FROM match_score.match AS m 
                            LEFT JOIN match_score.player_match_detail AS pmd ON m.id = pmd.match_id
                              JOIN match_score.player as p ON p.id = pmd.player_id
                                WHERE title LIKE ?''', (f'%{search}%',))

    flattened = {}
    for id, title, played_at, match_format_id, player_id, player_name in data:                    
        if id not in flattened:
            flattened[id] = (id, title, played_at, match_format_id, [], [])

        if player_id not in flattened:
            flattened[id][-2].append(player_id)

        if player_name not in flattened:
            flattened[id][5].append(player_name)

    return (PlayerMatch.from_query_result(*obj) for obj in flattened.values()) 


def all_team_matches(search: str = None):  
    if search is None:
        data = read_query(
                '''SELECT m.id as match_id, title, played_at, match_format_id, t.id as team_id, t.name as team_name
                          FROM match_score.match AS m 
                            LEFT JOIN match_score.team_match_detail AS tmd ON m.id = tmd.match_id
                              JOIN match_score.team as t ON t.id = tmd.team_id''')


    else:
        data = read_query(
                '''SELECT m.id as match_id, title, played_at, match_format_id, t.id as team_id, t.name as team_name
                          FROM match_score.match AS m 
                            LEFT JOIN match_score.team_match_detail AS tmd ON m.id = tmd.match_id
                              JOIN match_score.team as t ON t.id = tmd.team_id
                                WHERE title LIKE ?''', (f'%{search}%',))

    flattened = {}
    for match_id, title, played_at, match_format_id, team_id, team_name, in data:
        if match_id not in flattened:
            flattened[match_id] = (match_id, title, played_at, match_format_id, [], [])

        if team_id not in flattened:
            flattened[match_id][-2].append(team_id)

        if team_name not in flattened:
            flattened[match_id][5].append(team_name)

    return (TeamMatch.from_query_result(*obj) for obj in flattened.values())


def sort(categories: list[Match], *, attribute="title", reverse=False):
    if attribute == 'title':
        def sort_fn(m: Match): return m.title
    elif attribute == "match_format_id":
        def sort_fn(m: Match): return m.match_format_id
    elif attribute == "played_at":
        def sort_fn(m:Match): return m.played_at    
    else:
        def sort_fn(m: Match): return m.id

    return sorted(categories, key=sort_fn, reverse=reverse)


def get_with_players(id: int):
    match_data = read_query(
        'select id, title, played_at, match_format_id from match_score.match where id = ?', (id,))

    match = next((Match.from_query_result(*row) for row in match_data), None)

    if match is None:
        return None

    players_data = read_query(
        '''SELECT player_id, concat(p.first_name," ", p.second_name) as name, p.country_id, pmd.score
	FROM match_score.player p
		LEFT JOIN match_score.player_match_detail as pmd ON p.id = pmd.player_id
               WHERE match_id = ?''',
        (match.id,))

    return create_player_response_object(
        match,
        [PlayerMatchInfo.from_query_result(*row) for row in players_data])


def create_player_response_object(match: Match, players: list[PlayerMatchInfo]):
    return {
        'match_id': match.id,
        'title': match.title,
        'played_at': match.played_at,
        "format_id": match.match_format_id,
        'participants': players
    }


def get_with_teams(id: int):
    match_data = read_query(
        'select id, title, played_at, match_format_id from match_score.match where id = ?', (id,))
    match = next((Match.from_query_result(*row) for row in match_data), None)

    if match is None:
        return None

    players_data = read_query(
        '''SELECT t.id, t.name, tmd.score
	FROM match_score.team t
		LEFT JOIN match_score.team_match_detail as tmd ON t.id = tmd.team_id
               WHERE match_id = ?''',
        (match.id,))

    return create_team_response_object(
        match,
        [TeamMatchInfo.from_query_result(*row) for row in players_data])


def create_team_response_object(match: Match, teams: list[TeamMatchInfo]):
    return {
        'match_id': match.id,
        'title': match.title,
        'played_at': match.played_at,
        "format_id": match.match_format_id,
        'participants': teams
    }



def create_match_with_players(match: Match, participants: list[str]):
    
    participants = player_service.create_unknown_participants_profile(participants)

    players = [player_service.get_player_by_name(p) for p in participants]

    generated_id = insert_query(
        'INSERT INTO match_score.match (title, played_at, match_format_id) VALUES (?,?,?)',
        (match.title, match.played_at, match.match_format_id))
    match.id = generated_id

    for player in players:
        insert_query('INSERT INTO match_score.player_match_detail (player_id, match_id) VALUES (?,?)', (player.id, match.id))
    


def create_with_teams(match: Match, teams: list[str] ):
    generated_id = insert_query(
        'INSERT INTO match_score.match (title, played_at, match_format_id) VALUES (?,?,?)',
        (match.title, match.played_at, match.match_format_id))
    match.id = generated_id

    team_ids = [team_service.team_id(t) for t in teams]
    for team_id in team_ids:
        insert_query('INSERT INTO match_score.team_match_detail (team_id, match_id) VALUES (?,?)',(team_id, match.id))



def update_player_match_score(match_id: int, update: PlayerMatchDetailUpdate):

    for player, score in zip(update.player_ids, update.score):
        match = update_query("""UPDATE match_score.player_match_detail SET  
                                score = ? WHERE player_id = ? and match_id = ?""", (score, player, match_id))

    return match


def update_team_match_score(match_id: int, update: TeamMatchDetailUpdate):

    for team, score in zip(update.team_ids, update.score):
        match = update_query("""UPDATE match_score.team_match_detail SET  
                                score = ? WHERE team_id = ? and match_id = ?""", (score, team, match_id))

    return match


def exists(match_id: int):
    data = read_query('SELECT 1 from match where id = ?', (match_id,))

    return len(data) > 0


def get_match_players(match_id: int) -> set[int]:
    data = read_query(
        'SELECT player_id from match_detail where match_id = ?', (match_id,))

    return set(i[0] for i in data)


def update(match_id: int, match: MatchUpdate):
    result = update_query(
        '''UPDATE match SET
           title = ?, played_at = ?
           WHERE id = ? 
        ''',
        (match.title, match.played_at, match_id))

    return result


def send_email_to_match_users(participants: list[str], match: Match):
    linked_players = user_service.select_users_with_linked_players()

    for lp in linked_players:
        if f"{lp.player.first_name} {lp.player.second_name}" in participants:
            added_to_match_mail(lp, match)



def added_to_match_mail(details: LinkedUserDetails, match: Match):
    api_key = '9ebae64d6176dd19a3f36a58da793a96'
    api_secret = 'fcfc088b09bee693b6ff2988e9f5d1a9'
    mailjet = Client(auth=(api_key, api_secret), version='v3.1')
    data = {
        'Messages': [
        {
            "From": {
            "Email": "matchscore.team3@gmail.com",
            "Name": "Admin"
        },
            "To": [
            {
                "Email": f"{details.user_email}",
                "Name": f"{details.user_email}"
            }
            ],
            "Subject": "Added to match",
            "TextPart": f"You have been added to the {match.title} that is scheduled to start on {match.played_at}. Good luck!",
            }
        ]
    }
    mailjet.send.create(data=data)

#================================================ Testing below





