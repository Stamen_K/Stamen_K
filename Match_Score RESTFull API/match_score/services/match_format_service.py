from data.database import read_query, insert_query, update_query
from data.models import MatchFormat, MatchFormatUpdate
from mariadb import IntegrityError




def create(format: MatchFormat):
    generated_id = insert_query(
        '''INSERT INTO match_format (name, rules)
        VALUES(?,?)''',
        (format.name, format.rules))

    format.id = generated_id

    return format


def all_formats():
    return read_query("SELECT * from match_format")


def get_by_id(id: int):
    format_data = read_query(
            '''SELECT  id, name, rules
	            FROM match_format
                WHERE id = ?''', (id,))
    
    return next((MatchFormat.from_query_result(*row) for row in format_data), None)


def update(format_update: MatchFormatUpdate, format: MatchFormat): 
    format.name = format_update.name
    format.rules= format_update.rules

    update_query(
        '''UPDATE match_format SET
           name = ?, rules = ?
           WHERE id = ? ''',
        (format_update.name, format_update.rules, format.id))
    return format


def sort(categories: list[MatchFormat], *, attribute="name", reverse=False):
    if attribute == 'name':
        def sort_fn(f: MatchFormat): return f.name

    return sorted(categories, key=sort_fn, reverse=reverse)