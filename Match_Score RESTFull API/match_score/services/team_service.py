from data import database
from data.models import Team
from mariadb import IntegrityError


def create_team(team: Team):
    try:
        generated_id = database.insert_query("INSERT INTO team (name) values(?)", (team.name,))
        team.id = generated_id
    
    except IntegrityError:
        return None
    return team


def all_team():
    query = database.read_query("SELECT * from team")

    return (Team(id=t[0], name=t[1]) for t in query)


def get_team_by_id(id: int):
    query = database.read_query("SELECT * from team where id = ?", (id,))
    if not query: return None

    return Team(id=query[0][0], name=query[0][1])


def team_names():
    team_names = database.read_query("SELECT name from team")

    return [i[0] for i in team_names]


def team_id(name: str):
    id = database.read_query("SELECT id from team where name = ?", (name,))

    return id[0][0]


def delete_team(id: int):
    database.update_query("DELETE from team where id = ?", (id,))