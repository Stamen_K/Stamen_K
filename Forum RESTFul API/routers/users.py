from fastapi import APIRouter, Response, Header
from data.models import LogInfo
from services import users_service
from common.responses import NotFound, BadRequest, Unauthorized

users_router = APIRouter(prefix='/users')


@users_router.post('/login')
def login(credentials: LogInfo):
    user = users_service.find_user(credentials)

    if user: return {"token":users_service.create_token(credentials)}
    return Unauthorized('Wrong credentials')


@users_router.post('/register')
def register(credentials: LogInfo):
    registration = users_service.register(credentials)

    if registration: return registration
    return BadRequest('Credentials do not fit the requirements')

#if user is admin automatically returns info of all users otherwise only token user info
@users_router.get('/info')
def info(x_token: str = Header()):
    user_credentials = users_service.from_token(x_token)
    
    if not user_credentials: return Unauthorized('Unauthorized or expired token')
    user_data = users_service.find_user(user_credentials)

    if user_data.is_admin(): return users_service.all()
    return user_data


@users_router.put('/makeadmin/{username}')
def make_admin(username: str, x_token: str = Header()):
    target_user = users_service.find_user_by_username(username)
    auth_credentials = users_service.from_token(x_token)

    if not auth_credentials: return Unauthorized('Wrong Token')
    
    auth_user = users_service.find_user(auth_credentials)

    if not target_user: return NotFound('User not found')
    if not auth_user.is_admin(): return Unauthorized('Only admins can make other admins')

    return users_service.make_admin(target_user)
