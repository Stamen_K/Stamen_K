from datetime import date, datetime
from fastapi import APIRouter, Response, Header
from common.responses import NoContent, NotFound, BadRequest, InternalServerError, Unauthorized
from data.models import Topic, TopicUpdate
from services import categories_service, replies_service, topics_service, users_service


topics_router = APIRouter(prefix='/topics')

@topics_router.get('/')
def get_topics(sort: str | None = None, search: str | None = None, x_token: str | None = Header(default=None)):
    topics = topics_service.all(search)
    all_categories = categories_service.all()
    
    if x_token:
        user_auth = users_service.from_token(x_token)
        if not user_auth: return Unauthorized('Wrong or expired Token')     
        
        user = users_service.find_user(user_auth)   
        
        if user.is_admin():
            categories = all_categories
        else:
            categories = categories_service.nonprivate_categories_by_user(all_categories, user)
            topics = topics_service.remove_private_topics(categories, topics)
    
    elif not x_token: 
        categories = [c for c in all_categories if not c.private]
        topics = topics_service.remove_private_topics(categories, topics)
    
    if sort and (sort == 'asc' or sort == 'desc'):
        return topics_service.sort(topics, reverse=sort == 'desc')
    else:
        return topics


@topics_router.get('/{id}')
def get_topic_by_id(id: int, x_token: str | None = Header(default=None)):
    topic = topics_service.get_by_id(id)
    category = categories_service.get_by_id(topic.category_id)
    
    if not topic: 
        return NotFound('Not found')
    
    else:
        if not category.private:
            replies = topics_service.get_topic_replies(topic.id)
            return topics_service.create_response_object(topic, replies)
        
        elif category.private:
            if x_token:
                user_auth = users_service.from_token(x_token)
                if not user_auth: return Unauthorized('Wrong or expired token')
                
                user = users_service.find_user(user_auth)
                
                if not user.is_admin():
                    privilliged = categories_service.see_read_access(category.id)
                    if user.id not in privilliged: return Unauthorized('You dont have access to this topic')
                
                replies = topics_service.get_topic_replies(topic.id)
                return topics_service.create_response_object(topic, replies)
            
            elif not x_token: return Unauthorized('This topic is private')


@topics_router.post('/')            
def create_topic(topic: Topic, x_token: str = Header()):
    user_auth = users_service.from_token(x_token)
    
    if user_auth is None:
        return Unauthorized('You cannot create a topic!')
    
    user = users_service.find_user(user_auth)
    topic.user_id = user.id
    topic.created_on = datetime.utcnow()
    category = categories_service.get_by_id(topic.category_id)

    if not category: return BadRequest(f'Category {topic.category_id} does not exist')
    if category.locked: return Unauthorized('Cannot add topic. This category is locked')  
    if not user.is_admin() and topic.locked: return Unauthorized('Only admins can create a locked topic!')
    
    if category.private: 
        full_access_users = categories_service.see_full_access(category.id)
        if not user.is_admin() and user.id not in full_access_users: return Unauthorized('You do not have full access to this category')

    return topics_service.create(topic)


@topics_router.put('/{id}')
def update_topic(id: int, data: TopicUpdate, x_token: str = Header()):
    user_auth = users_service.from_token(x_token)
   
    if not user_auth:
        return Unauthorized('You are not logged in!')
    
    user = users_service.find_user(user_auth)
    topic = topics_service.get_by_id(id)
    
    if topic is None:
        return NotFound()

    if topic.user_id == user.id or user.is_admin():
        topic = topics_service.update(data, topic, user)
        return topic 

    return Unauthorized('You cannot modify this topic! You are not an author or admin!')