from fastapi import APIRouter
from fastapi import Header
from data.models import Category, CategoryUpdatePrivate, CategoryUpdateLocked, Privilliged_Users
from services import users_service, topics_service, categories_service
from common.responses import NotFound, Unauthorized, BadRequest




categories_router = APIRouter(prefix='/categories')



# WORKS
@categories_router.get('/')
def get_categories(sort: str | None = None, sort_by: str | None = None, search: str | None = None, x_token: str | None = Header(default=None)):
    categories = list(categories_service.all(search))
    
    if x_token: 
        user_auth = users_service.from_token(x_token)
        if not user_auth: return Unauthorized('Unauthorized or expired token!')
        user = users_service.find_user(user_auth)
        
        if not user.is_admin(): 
            categories = categories_service.nonprivate_categories_by_user(categories, user)
    if not x_token: 
        categories = [c for c in categories if not c.private]   

    if sort and (sort == 'asc' or sort == 'desc'):
        return categories_service.sort(categories, reverse=sort == 'desc', attribute=sort_by)
    else:
        return categories


# WORKS                                             
@categories_router.get('/{id}')                                  #Works fully implemented
def get_category_by_id(id: int, x_token: str | None = Header(default=None)):
    category = categories_service.get_by_id(id)

    if category is None:
        return NotFound('Category not Found')
    
    if category.private:
        user_auth = users_service.from_token(x_token)
    
        if not user_auth:
            return Unauthorized('Unauthorized or expired token!')
    
        user = users_service.find_user(user_auth)
        privilliged = categories_service.see_read_access(id)

        if user.id not in privilliged and not user.is_admin():
            return Unauthorized('You don not have access to the category')

    topics = topics_service.get_by_category(id)

    return categories_service.create_response_object(category, topics)


@categories_router.post('/')
def create_category(category: Category, x_token: str = Header()):
    user_auth = users_service.from_token(x_token)
    
    if user_auth is None:
        return Unauthorized('Unauthorized or expired token!')
    
    user = users_service.find_user(user_auth)
    category.user_id = user.id    
    
    if not user.is_admin(): return Unauthorized('You cannot create a category!')
    
    created_category = categories_service.create(category)
    return created_category


@categories_router.put('/lock_unlock/{id}')
def category_lock_unlock(id: int, data: CategoryUpdateLocked, x_token: str = Header()):
    user_auth = users_service.from_token(x_token)
    
    if not user_auth:
        return Unauthorized('Wrong or Expired Token')

    user = users_service.find_user(user_auth)
    category = categories_service.get_by_id(id)
    
    if category is None:
        return NotFound('Category not Found')
    
    if user.is_admin():
        category = categories_service.admin_lock_unlock(data, category)
        return category

    return Unauthorized('You are not the author of this category!')



@categories_router.put('/private_or_not/{id}')
def category_private_or_not(id: int, data: CategoryUpdatePrivate, x_token: str = Header()):
    user_auth = users_service.from_token(x_token)

    if not user_auth:
        return Unauthorized('Wrong or Expired Token')

    user = users_service.find_user(user_auth)
    category = categories_service.get_by_id(id)

    if category is None:
        return NotFound('Category not Found')
    
    if user.is_admin():
        category = categories_service.admin_private_or_not(data, category)
        return category

    return Unauthorized('You are not the author of this category!')


@categories_router.post('/access')
def add_private_users(data: Privilliged_Users, x_token: str = Header()):
    auth_credentials = users_service.from_token(x_token)
    
    if not auth_credentials: return Unauthorized('Wrong or Expired Token')
    auth_user = users_service.find_user(auth_credentials)
    if not auth_user.is_admin(): return Unauthorized('You are not admin')

    category = categories_service.get_by_id(data.cat_id)
    if not category: return NotFound('Category not Found')

    if not category.private: return BadRequest('The category is not private')   

    if not users_service.validate_users(data.speical_users): return BadRequest('User does not exist')

    added_users = categories_service.add_users_to_private(data)

    return added_users


@categories_router.put('/full_access')
def promote_to_full_access(data: Privilliged_Users, x_token: str = Header()):
    category = categories_service.get_by_id(data.cat_id)
    auth_credentials = users_service.from_token(x_token)
    
    if not auth_credentials:
        return Unauthorized('Wrong or Expired Token')
    
    auth_user = users_service.find_user(auth_credentials)

    if not auth_user.is_admin():
        return Unauthorized('You are not admin')
    
    if not category:
        return NotFound('Category not Found')

    if not users_service.validate_users(data.speical_users):
        return BadRequest('User does not exist')

    added_users = categories_service.give_full_access(data)

    return added_users


@categories_router.put('/revoke_write_access')
def revoke_write_access(data: Privilliged_Users, x_token: str = Header()):
    category = categories_service.get_by_id(data.cat_id)
    auth_credentials = users_service.from_token(x_token)
    
    if not auth_credentials:
        return Unauthorized('Wrong or Expired Token')
    
    auth_user = users_service.find_user(auth_credentials)

    if not auth_user.is_admin():
        return Unauthorized('You are not admin')
    
    if not category:
        return NotFound('Category not Found')

    read_access_users = categories_service.revoke_write_access(data) 

    return read_access_users


@categories_router.delete('/revoke_access')
def complete_revoke(data: Privilliged_Users, x_token: str = Header()):
    category = categories_service.get_by_id(data.cat_id)
    auth_credentials = users_service.from_token(x_token)
    
    if not auth_credentials:
        return Unauthorized('Wrong or Expired Token')
    
    auth_user = users_service.find_user(auth_credentials)

    if not auth_user.is_admin():
        return Unauthorized('You are not admin')

    if not category:
        return NotFound('Category not Found')

    access_users = categories_service.completely_revoke_access(data) 

    return access_users
