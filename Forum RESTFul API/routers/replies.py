from datetime import date, datetime
from fastapi import APIRouter, Response, Header
from data.models import Reply
from services import replies_service, topics_service, users_service, categories_service
from common.responses import BadRequest, InternalServerError, NoContent, NotFound, Unauthorized



replies_router = APIRouter(prefix='/replies')

@replies_router.get('/{topic_id}')
def get_replies(topic_id: int, x_token: str | None = Header(default=None)):
    
    topic = topics_service.get_by_id(topic_id)
    if topic is None: return NoContent('No such topic')
    
    replies = replies_service.all(topic_id)
    if replies is None: return NoContent('No replies to view')

    category = categories_service.get_by_id(topic.category_id)

    if not x_token and category.private: return Unauthorized('You do not have permission to view this replies')

    if x_token:
        user_auth = users_service.from_token(x_token)
        if not user_auth: return Unauthorized('Wrong or expired token')

        user = users_service.find_user(user_auth)
        if user.is_admin(): return replies

        if category.private:
            privilliged = categories_service.see_read_access(category.id)
            if user.id not in privilliged: return Unauthorized('You do not have permission to view this replies')
    
    return replies


@replies_router.get('/{topic_id}/{id}')
def get_reply_by_id(topic_id: int, id: int, x_token: str | None = Header(default=None)):
    
    topic = topics_service.get_by_id(topic_id)
    if topic is None: return NoContent('No such topic')

    reply = replies_service.get_by_id(id)
    if reply is None: return NotFound('No replies to view')

    category = categories_service.get_by_id(topic.category_id)

    if not x_token and category.private(): return Unauthorized('You do not have permission to view this replies')

    if x_token:
        user_auth = users_service.from_token(x_token)
        if not user_auth: return Unauthorized('Wrong or expired token')

        user = users_service.find_user(user_auth)
        if user.is_admin(): return reply

        if category.private:
            privilliged = categories_service.see_read_access(category.id)
            if user.id not in privilliged: return Unauthorized('You do not have permission to view this replies')
    
    return reply
  

@replies_router.post('/')
def create_reply(reply: Reply, x_token: str = Header()):

    user_auth = users_service.from_token(x_token)
    if not user_auth: return Unauthorized('You cannot create a reply!')
    user = users_service.find_user(user_auth)

    if not topics_service.exists(reply.topic_id): return BadRequest('Topic {reply.topic_id} does not exist') 
    
    
    topic = topics_service.get_by_id(reply.topic_id)

    reply.user_id = user.id
    reply.created_on = datetime.utcnow() 
    if user.is_admin(): return replies_service.create(reply)

    if topic.locked: return Unauthorized('You cannot add a reply to this topic!')
    if reply.votes != 0: return Unauthorized('You cannot create reply with votes greater than 0!')
    
    category = categories_service.get_by_id(topic.category_id)

    if category.private:
        privilliged = categories_service.see_full_access(category.id)
        if user.id not in privilliged: return Unauthorized('You do not have permission to write in this category')
    
    return replies_service.create(reply)
    

@replies_router.put('/upvote/{id}')
def upvote_reply(id: int, x_token: str = Header()):
    
    reply = replies_service.get_by_id(id)
    if not reply: return NotFound('No such reply')

    user_auth = users_service.from_token(x_token)
    if not user_auth: return Unauthorized('Wrong Token')
    
    user = users_service.find_user(user_auth)

    topic = topics_service.get_by_id(reply.topic_id)
    if not topic: return BadRequest('Topic does not exist')

    category = categories_service.get_by_id(topic.category_id)

    if not user.is_admin() and category.private:
        privilliged = categories_service.see_full_access(category.id)
        if user.id not in privilliged: return Unauthorized('You cannot upvote this reply')
    
    if replies_service.has_voted_up(id, user):
        return Unauthorized('You cannot upvote again')
    if replies_service.has_voted_down(id, user):
        replies_service.delete_downvote(id, user)
        return replies_service.upvote2(id, user, 1, 'up')
    return replies_service.upvote(id, user, 1, 'up') 


@replies_router.put('/downvote/{id}')
def downvote_reply(id: int, x_token: str = Header()):

    reply = replies_service.get_by_id(id)
    if not reply: return NotFound('No such reply')

    user_auth = users_service.from_token(x_token)
    if not user_auth: return Unauthorized('Wrong Token')
    
    user = users_service.find_user(user_auth)

    topic = topics_service.get_by_id(reply.topic_id)
    if not topic: return BadRequest('Topic does not exist')

    category = categories_service.get_by_id(topic.category_id)

    if not user.is_admin() and category.private:
        privilliged = categories_service.see_full_access(category.id)
        if user.id not in privilliged: return Unauthorized('You do not have permission to write in this category')
    
    if replies_service.has_voted_down(id, user):
        return Unauthorized('You cannot downvote again')
    if replies_service.has_voted_up(id, user):
        replies_service.delete_upvote(id, user)
        return replies_service.downvote2(id, user, -1, 'down')
    return replies_service.downvote(id, user, -1, 'down')    