from fastapi import APIRouter, Response, Header
from services import users_service
from services import conversations_service
from data.models import Message
from common.responses import NotFound, Unauthorized

conversations_router = APIRouter(prefix='/conversations')


@conversations_router.get('/')
def all_conversations(x_token: str = Header()):
    user_crendentials = users_service.from_token(x_token)
    if not user_crendentials: return Unauthorized('Unauthorized or Expired Token')
    
    user = users_service.find_user(user_crendentials)

    return conversations_service.all(user)


@conversations_router.get('/{username}')
def get_by_username(username: str, x_token: str = Header()):
    search = users_service.find_user_by_username(username)
    curr_user = users_service.from_token(x_token)

    if not search: return NotFound('User not found')
    if not curr_user: return Unauthorized('Unauthorized or Expired Token')

    return conversations_service.get_convo_by_user(curr_user=curr_user, search=search)


@conversations_router.post('/message/{username}')
def message(username: str, message: Message, x_token: str = Header()):
    sender_credentials = users_service.from_token(x_token)
    
    if not sender_credentials: return Unauthorized('Unauthorized or Expired Token')
    
    sender = users_service.find_user(sender_credentials)
    receiver = users_service.find_user_by_username(username)
    
    if not receiver: return NotFound('User not found')

    return conversations_service.message(sender, message.text, receiver)

    
