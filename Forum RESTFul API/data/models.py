from datetime import date, datetime
from typing import Optional
from pydantic import BaseModel, constr
from enum import Enum



#=================================== Categories
class Category(BaseModel):
    id: int | None
    name: str
    locked: bool = False
    private: bool = False
    user_id: int | None


    @classmethod
    def from_query_result(cls, id, name, locked, private, user_id):
        return cls(
            id = id,
            name = name,
            locked = locked,
            private = private,
            user_id = user_id)


class CategoryUpdatePrivate(BaseModel):
    name: str
    private: bool


class CategoryUpdateLocked(BaseModel):
    name: str
    locked: bool


class Privilliged_Users(BaseModel):
    cat_id: int | None
    speical_users: list[int]


class CategoryResponseModel(BaseModel):
    id: int | None
    name: str
    locked: bool = False
    private: bool = False
    user_id: int | None
    topics: list
    

#=================================== Topic & Conversation
class Topic(BaseModel):
    id: int | None
    title: str
    text: str
    user_id: int = None
    category_id: int
    created_on: date = None
    replies: list[int] = []
    locked: bool = False
    best_reply_id: int = 0

    @classmethod
    def from_query_result(cls, id, title, text, categories_id, users_id, locked, created_on, best_reply_id, replies_id=[]):
        return cls(
            id = id,
            title = title,
            text = text,
            user_id = users_id,
            category_id = categories_id,
            locked = locked,
            created_on = created_on,
            best_reply_id = best_reply_id,
            replies = replies_id)


class TopicUpdate(BaseModel):
    title: str
    text: str
    category_id: int 
    locked: bool
    best_reply_id: int 


class Reply(BaseModel):
    id: int | None
    text: str
    topic_id: int
    user_id: int = None
    created_on: date = None
    votes: int = 0

    @classmethod
    def from_query_result(cls, id, text, topic_id, user_id, upvotes, created_on):
        return cls(
            id = id,
            text = text,
            topic_id = topic_id,
            user_id = user_id,
            votes = upvotes,
            created_on = created_on)


class TopicResponseModel(BaseModel):
    id: int | None
    title: str
    text: str
    user_id: int
    category_id: int
    created_on: date
    replies: list[Reply] 
    locked: bool
    best_reply_id: int


class Conversation(BaseModel):
    sender: str
    text: str
    receiver: str


class Message(BaseModel):
    text: str


class Reply(BaseModel):
    id: int | None
    text: str
    topic_id: int
    user_id: int = None
    created_on: date = None
    votes: int = 0

    @classmethod
    def from_query_result(cls, id, text, topic_id, user_id, upvotes, created_on):
        return cls(
            id = id,
            text = text,
            topic_id = topic_id,
            user_id = user_id,
            votes = upvotes,
            created_on = created_on)

#=================================== User
class Role(str, Enum):
    adm = 'admin'
    usr = 'user'


class User(BaseModel):
    id: int | None
    username: constr(regex='^\w{2,30}$')
    password: str
    role: Role

    def is_admin(self):
        return self.role == 'admin'


class LogInfo(BaseModel):
    username: constr(regex='^\w{2,30}$')
    password: str





