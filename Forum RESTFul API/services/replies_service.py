from data.models import Reply, User
from data.database import insert_query, read_query, update_query


def all(topic_id):  
    data = read_query(
            '''SELECT id, text, topics_id, users_id, upvotes, created
               FROM replies where topics_id = ?''', (topic_id,))
    
    return (Reply.from_query_result(*row) for row in data)


def get_by_id(id: int):
    data = read_query(
        '''SELECT id, text, topics_id, users_id, upvotes, created 
        FROM replies 
        WHERE id = ?''', (id,))

    return next((Reply.from_query_result(*row) for row in data), None)


def create(reply: Reply):
    generated_id = insert_query(
        '''INSERT INTO replies (text, topics_id, users_id, upvotes, created)
        VALUES(?,?,?,?,?)''',
        (reply.text, reply.topic_id, reply.user_id, reply.votes, reply.created_on))

    reply.id = generated_id

    return reply


def get_by_topic(topic_id: int):
    data = read_query(
        'SELECT * FROM replies WHERE topics_id = ?', (topic_id,))

    return (Reply.from_query_result(*row) for row in data)


def has_voted_up(reply_id: int, user: User):
    check = update_query('select * from votes where (users_id =? and replies_id =? and vote = ?)',(user.id, reply_id, 'up'))
    
    return check 

def has_voted_down(reply_id: int, user: User):
    check = update_query('select * from votes where (users_id =? and replies_id =? and vote = ?)',(user.id, reply_id, 'down'))

    return check

def upvote(reply_id: int, user: User, score: int, vote: str):
    votes = insert_query('''INSERT INTO votes (score, users_id, replies_id, vote)
        VALUES(?,?,?,?)''', (score, user.id, reply_id, vote))
    replies = update_query('update replies set upvotes = upvotes + 1 where id= ?', ( reply_id,))

    return get_by_id(reply_id)

def upvote2(reply_id: int, user: User, score: int, vote: str):
    votes = insert_query('''INSERT INTO votes (score, users_id, replies_id, vote)
        VALUES(?,?,?,?)''', (score, user.id, reply_id, vote))
    replies = update_query('update replies set upvotes = upvotes + 2 where id= ?', ( reply_id,))

    return get_by_id(reply_id)

def downvote(reply_id: int, user: User, score: int, vote: str):
    votes = insert_query('''INSERT INTO votes (score, users_id, replies_id, vote)
        VALUES(?,?,?,?)''', (score, user.id, reply_id, vote))
    replies = update_query('update replies set upvotes = upvotes - 1 where id= ?', ( reply_id,))

    return get_by_id(reply_id)

def downvote2(reply_id: int, user: User, score: int, vote: str):
    votes = insert_query('''INSERT INTO votes (score, users_id, replies_id, vote)
        VALUES(?,?,?,?)''', (score, user.id, reply_id, vote))
    replies = update_query('update replies set upvotes = upvotes - 2 where id= ?', ( reply_id,))

    return get_by_id(reply_id)

def delete_upvote(reply_id: int, user: User):
    update_query('delete from votes where (users_id =? and replies_id =? and vote = ?)',(user.id, reply_id, 'up'))

def delete_downvote(reply_id: int, user: User):
    update_query('delete from votes where (users_id =? and replies_id =? and vote = ?)',(user.id, reply_id, 'down'))