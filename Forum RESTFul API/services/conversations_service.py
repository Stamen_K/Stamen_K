from data import database
from data.models import Conversation, LogInfo, User


def all(user: User):
    all = database.read_query('''select s.username, r.username from conversations 
    join users r on receiver = r.id 
    join users s on sender = s.id where sender = ? or receiver = ?''', (user.id, user.id))
    
    if not all: return []
    
    result = set([u[0] for u in all if u[0] != user.username] + [u[1] for u in all if u[1] != user.username])
    return result


def get_convo_by_user(curr_user: LogInfo, search: User):
    messages = database.read_query('''select s.username, text, r.username from conversations 
    join users r on receiver = r.id 
    join users s on sender = s.id where s.username = ? and r.username = ? or s.username = ? and r.username = ?''',
    (curr_user.username, search.username, search.username, curr_user.username))

    result = [Conversation(sender=m[0], text=m[1], receiver=m[2]) for m in messages]
    return result


def message(sender: User, text: str, receiver: User):
    message = database.insert_query('insert into conversations(sender, text, receiver) values(?,?,?)',(sender.id, text, receiver.id))

    return Conversation(sender=sender.username, text=text, receiver=receiver.username)