from common.responses import Unauthorized
from data.models import Category, Reply, Topic, TopicResponseModel, TopicUpdate, User
from data.database import insert_query, read_query, update_query

def all(search: str = None):  
    if search is None:
        data = read_query(
                '''SELECT  t.id, title, t.text, categories_id, t.users_id,
                    locked, t.created, best_reply, r.id
                    FROM topics as t
                    LEFT JOIN replies as r
                    ON t.id = r.topics_id''')
    else:
        data = read_query(
                '''SELECT  t.id, title, t.text, categories_id, t.users_id,
                    locked, t.created, best_reply, r.id
                    FROM topics as t
                    LEFT JOIN replies as r
                    ON t.id = r.topics_id
                    WHERE title LIKE ?''', (f'%{search}%',))

    flattened = {}
    for id, title, text, categories_id, users_id,\
        locked, created, best_reply, reply_id in data:

        if id not in flattened:
            flattened[id] = (
            id, title, text, categories_id, users_id,
            locked, created, best_reply, [])

        if reply_id is not None:
            flattened[id][-1].append(reply_id)

    return (Topic.from_query_result(*obj) for obj in flattened.values())
    

def sort(lst: list[Topic], reverse=False):
    return sorted(
        lst,
        key=lambda t: t.created_on,
        reverse=reverse)


def get_by_id(id: int):
    topic_data = read_query(
            '''SELECT  t.id, t.title, t.text, t.categories_id,
                t.users_id, t.locked, t.created, t.best_reply
	            FROM topics as t
                WHERE t.id = ?''', (id,))
    
    return next((Topic.from_query_result(*row) for row in topic_data), None)


def get_topic_replies(topic_id: int) -> list[Reply]:
    replies_data = read_query(
        '''SELECT r.id, r.text, r.topics_id,
               r.users_id, r.upvotes, r.created
               FROM  replies as r
               WHERE topics_id = ?''', (topic_id,))

    return [Reply.from_query_result(*row) for row in replies_data]


def create(topic: Topic):
    generated_id = insert_query('''insert into topics values (?, ?, ?, ?, ?, ?, ?, ?)''',
    (0, topic.title, topic.text, topic.category_id, topic.user_id, topic.locked, topic.created_on, topic.best_reply_id))

    topic.id = generated_id

    return topic

def update(topic_update: TopicUpdate, topic: Topic, user: User): 

    topic.title = topic_update.title
    topic.text= topic_update.text
    topic.locked = topic_update.locked if user.is_admin() else topic.locked
    topic.category_id = topic_update.category_id
    topic.best_reply_id = topic_update.best_reply_id

    update_query(
        '''UPDATE topics SET
           title = ?, text = ?, categories_id = ?,
           locked = ?, best_reply = ?
           WHERE id = ? ''',
        (topic_update.title, topic_update.text, topic_update.category_id,\
         topic.locked, topic_update.best_reply_id, topic.id))

    if not user.is_admin() and topic.locked != topic_update.locked:
        return Unauthorized('You cannot lock/unlock topic! You are not an admin!')
    else:
        return topic
    
    
def exists(id: int):

    return any(read_query('select * from topics where id = ?',(id,)))


def create_response_object(topic: Topic, topic_replies: list[Reply]):
    
    return TopicResponseModel(
        id = topic.id,
        title = topic.title,
        text = topic.text,
        user_id = topic.user_id,
        category_id = topic.category_id,
        created_on = topic.created_on, 
        replies = topic_replies,
        locked = topic.locked,
        best_reply_id = topic.best_reply_id)
    
    
def get_by_category(category_id: int):
    data = read_query(
        'SELECT id FROM topics WHERE categories_id = ?', (category_id,))

    return [id[0] for id in data]


def remove_private_topics(categories: list[Category], topics: list[Topic]):
    cat_ids = [cat.id for cat in categories]
    return [t for t in topics if t.category_id in cat_ids]



