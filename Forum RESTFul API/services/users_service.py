from datetime import datetime, timedelta
from data.models import LogInfo, User
from data import database
import jwt
from hashlib import blake2s
from data.secrets import SALT, SECRET_KEY, ALGO
from mariadb import IntegrityError


def _hash(password: str):
    password += SALT
    return blake2s(password.encode()).hexdigest()

#Return User class w/ role and id from credentials or None
def find_user(credentials: LogInfo):
    h_password = _hash(credentials.password)
    
    check = database.read_query('select id, username, password, role from users where username = ? and password = ?', (credentials.username, h_password))
    if not check: return

    check = check[0]

    return User(id=check[0], username=check[1], password=credentials.password, role=check[3])


def all():
    users = database.read_query('select id, username, password, role from users')
    if not users: return

    result = [User(id=u[0], username=u[1], password=u[2], role=u[3]) for u in users]
    
    return result

#Creates token from User Class
def create_token(user: User):
    EXPIRATION = datetime.utcnow() + timedelta(minutes=60)
    credentials = {"username":user.username, "password":user.password, "exp":EXPIRATION}
    
    token = jwt.encode(credentials, SECRET_KEY, algorithm=ALGO)

    return token

#Returns user credentials from token or None
def from_token(token: str):
    try:
        decoded = jwt.decode(token, SECRET_KEY, algorithms=ALGO)
    except jwt.exceptions.ExpiredSignatureError: return 
    except jwt.exceptions.InvalidSignatureError: return
    except jwt.exceptions.DecodeError: return

    return LogInfo(username=decoded['username'], password=decoded['password'])


def register(credentials: LogInfo):
    h_password = _hash(credentials.password)
    default_role = 'user'
    
    try:
        generated_id = database.insert_query('insert into users(username, password, role) values(?,?,?)', 
        (credentials.username, h_password, default_role))
    except IntegrityError:
        return 

    return User(id=generated_id, username=credentials.username, password=credentials.password, role=default_role)


def find_user_by_username(username: str):
    user = database.read_query('select id, username, password, role from users where username = ?', (username,))
    if not user: return

    user = user[0]
    return User(id=user[0], username=user[1], password=user[2], role=user[3])


def make_admin(user: User):
    new_admin = database.update_query('update users set role = ? where id = ?',('admin', user.id))
    user.role = 'admin'

    return user

def validate_users(special_users: list[User]):
    data = database.read_query('select id from users')
    existing_users = [i[0] for i in data]

    for id in special_users:
        if id not in existing_users:
            return False

    return True