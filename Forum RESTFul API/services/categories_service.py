from data.database import insert_query, read_query, update_query
from data.models import Category, CategoryUpdatePrivate, CategoryUpdateLocked, CategoryUpdateLocked, CategoryUpdatePrivate, Privilliged_Users, CategoryResponseModel, User





def all(search: str = None):
    if search is None:
        data = read_query(
                """SELECT id, name, locked, private, user_id FROM categories""")
    else:
        data = read_query(
                """SELECT id, name, locked, private, user_id FROM categories
                    WHERE name LIKE """) (f"%{search}%",)

    flattened = {}
    for id, name, locked, private, user_id in data:

        if id not in flattened:
            flattened[id] = (id, name, locked, private, user_id)

    return [Category.from_query_result(*obj) for obj in flattened.values()]


def get_by_id(id: int):                                                                 
    data = read_query("SELECT id, name, locked, private, user_id FROM categories WHERE id = ?", (id,))

    return next((Category.from_query_result(*row) for row in data), None)


def exists(id: int):
    return any(
        read_query(
            "SELECT id, name FROM categories WHERE id = ?",
            (id,)))


def create(category: Category):
    generated_id = insert_query("INSERT INTO categories VALUES(?, ?, ?, ?, ?)",
        (0, category.name, category.locked , category.private, category.user_id))

    category.id = generated_id

    return category


def sort(categories: list[Category], *, attribute="name", reverse=False):
    if attribute == 'name':
        def sort_fn(c: Category): return c.name
    elif attribute == "locked":
        def sort_fn(c: Category): return c.locked
    elif attribute == "private":
        def sort_fn(c:Category): return c.private    
    else:
        def sort_fn(c: Category): return c.id

    return sorted(categories, key=sort_fn, reverse=reverse)


def admin_lock_unlock(new_category: CategoryUpdateLocked, old_category: Category):
    update_query(
        """UPDATE categories SET
           locked = ?
           WHERE id = ? """,
        (new_category.locked, old_category.id))
    
    old_category.locked= new_category.locked

    return old_category


def admin_private_or_not(new_category: CategoryUpdatePrivate, old_category: Category):
    update_query(
        """UPDATE categories SET
           private = ?
           WHERE id = ? """,
        (new_category.private, old_category.id))
    
    old_category.private= new_category.private

    return old_category


def create_response_object(category: Category, category_topics: list[int]):
    
    return CategoryResponseModel(
        id = category.id,
        name = category.name,
        locked = category.locked,
        private = category.private,
        user_id = category.user_id,
        topics = category_topics)


def add_users_to_private(data: Privilliged_Users):
    for id in data.speical_users:
        insert_query("INSERT INTO users_access (users_id, categories_id, access_level) VALUES(?,?,?)",(id, data.cat_id, 1))
    return data.speical_users


def give_full_access(data: Privilliged_Users):
    for id in data.speical_users:
        update_query("UPDATE users_access SET access_level = 2 WHERE categories_id = ? AND users_id = ?",(data.cat_id, id))
    return data.speical_users


def see_read_access(cat_id: int):
    users = read_query("SELECT users_id FROM users_access WHERE categories_id = ?", (cat_id,))
    result = [i[0] for i in users]

    return result


def see_full_access(cat_id: int):
    users = read_query("SELECT users_id FROM users_access WHERE categories_id = ? AND access_level = 2", (cat_id,))
    result = [i[0] for i in users]

    return result


def revoke_write_access(data: Privilliged_Users):
    for id in data.speical_users:
        update_query("UPDATE users_access SET access_level = 1 WHERE categories_id = ? AND users_id = ?", (data.cat_id, id))
    return see_read_access(data.cat_id)


def completely_revoke_access(data: Privilliged_Users):
    for id in data.speical_users:
        update_query("DELETE from users_access WHERE categories_id = ? AND users_id = ?", (data.cat_id, id))
    
    read_access = see_read_access(data.cat_id)
    full_access = see_full_access(data.cat_id)

    return read_access + full_access


def nonprivate_categories_by_user(categories: list[Category], user: User):
    for c in categories:
        if c.private:
            privilliged = see_read_access(c.id)
            if user.id not in privilliged: categories.remove(c)
    
    return categories