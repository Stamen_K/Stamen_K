from unittest.mock import patch
import unittest
from common.responses import BadRequest, NotFound, Unauthorized
from routers import users
from data.models import User, LogInfo


class UsersRouter_Should(unittest.TestCase):

    TEST_USERS = [(1, 'john', 'john1', 'admin'), (2, 'alice', 'alice2', 'user')]
    
    
    @patch('routers.users.users_service')
    def test_logIn_returnsCorrectly(self, mock_service):
        mock_service.find_user.return_value = User(id=1, username='john', password='john1', role='admin')
        mock_service.create_token.return_value = 'johnstoken'

        result = users.login(LogInfo(username='john', password='john1'))
        expected = {"token": 'johnstoken' }

        self.assertEqual(result, expected)
    

    @patch('routers.users.users_service')
    def test_logIn_returnsCorrectly_whenNoUser(self, mock_service):
        mock_service.find_user.return_value = None
        mock_service.create_token.return_value = 'johnstoken'

        result = users.login(LogInfo(username='john', password='john1'))
        expected_type = Unauthorized

        self.assertIsInstance(result, expected_type)
    

    @patch('routers.users.users_service')
    def test_registration_returnsCorrectly(self, mock_service):
        mock_service.register.return_value = User(id=1, username='john', password='john1', role='admin')

        result = users.register(LogInfo(username='john', password='john1'))
        expected = User(id=1, username='john', password='john1', role='admin')

        self.assertEqual(result, expected)
    

    @patch('routers.users.users_service')
    def test_registration_returnsCorrectly_whenIncorrectUser(self, mock_service):
        mock_service.register.return_value = None

        result = users.register(LogInfo(username='john', password='john1'))
        expected_type = BadRequest

        self.assertIsInstance(result, expected_type)
    

    @patch('routers.users.users_service')
    def test_info_returnsCorrectly_whenNotAdmin(self, mock_service):
        mock_service.from_token.return_value = LogInfo(username='alice', password='alice2')
        mock_service.find_user.return_value = User(id=2, username='alice', password='alice2', role='user')

        result = users.info("test_token")
        expected = User(id=2, username='alice', password='alice2', role='user')

        self.assertEqual(result, expected)
    

    @patch('routers.users.users_service')
    def test_info_returnsCorrectly_whenAdmin(self, mock_service):
        mock_service.from_token.return_value = LogInfo(username='john', password='john1')
        mock_service.find_user.return_value = User(id=1, username='john', password='john1', role='admin')
        mock_service.all.return_value = [User(id=u[0], username=u[1], password=u[2], role=u[3]) for u in UsersRouter_Should.TEST_USERS]

        result = users.info("test_token")
        expected = [User(id=u[0], username=u[1], password=u[2], role=u[3]) for u in UsersRouter_Should.TEST_USERS]

        self.assertEqual(result, expected)
    

    @patch('routers.users.users_service')
    def test_info_returnsCorrectly_whenWrongToken(self, mock_service):
        mock_service.from_token.return_value = None

        result = users.info("wrong_token")
        expected_type = Unauthorized

        self.assertIsInstance(result, expected_type)
    

    @patch('routers.users.users_service')
    def test_makeAdmin_returnsCorrectly(self, mock_service):
        target_user = User(id=2, username='alice', password='alice2', role='user')
        auth_log_info = LogInfo(username='alice', password='alice2')
        auth_user = User(id=1, username='john', password='john1', role='admin')
        
        mock_service.find_user_by_username.return_value = target_user
        mock_service.from_token.return_value = auth_log_info
        mock_service.find_user.return_value = auth_user
        mock_service.make_admin.return_value = User(id=2, username='alice', password='alice2', role='admin')

        result = users.make_admin('alice', 'fake_token')
        expected = User(id=2, username='alice', password='alice2', role='admin')

        self.assertEqual(result, expected)
    

    @patch('routers.users.users_service')
    def test_makeAdmin_whenWrongToken(self, mock_service):
        target_user = User(id=2, username='alice', password='alice2', role='user')
        auth_log_info = None
        
        mock_service.find_user_by_username.return_value = target_user
        mock_service.from_token.return_value = auth_log_info

        result = users.make_admin('alice', 'fake_token')
        expected_type = Unauthorized

        self.assertIsInstance(result, expected_type)
    

    @patch('routers.users.users_service')
    def test_makeAdmin_whenNoSuchUser(self, mock_service):
        target_user = None
        auth_log_info = LogInfo(username='alice', password='alice2')
        auth_user = User(id=1, username='john', password='john1', role='admin')
        
        mock_service.find_user_by_username.return_value = target_user
        mock_service.from_token.return_value = auth_log_info
        mock_service.find_user.return_value = auth_user

        result = users.make_admin('alice', 'fake_token')
        expected_type = NotFound

        self.assertIsInstance(result, expected_type)
    

    @patch('routers.users.users_service')
    def test_makeAdmin_whenAuthNotAdmin(self, mock_service):
        target_user = User(id=2, username='alice', password='alice2', role='user')
        auth_log_info = LogInfo(username='alice', password='alice2')
        auth_user = User(id=2, username='alice', password='alice2', role='user')
        
        mock_service.find_user_by_username.return_value = target_user
        mock_service.from_token.return_value = auth_log_info
        mock_service.find_user.return_value = auth_user
        mock_service.make_admin.return_value = User(id=2, username='alice', password='alice2', role='admin')

        result = users.make_admin('alice', 'fake_token')
        expected_type = Unauthorized

        self.assertIsInstance(result, expected_type)