
import datetime
from unittest import mock
from unittest.mock import patch
import unittest
from data.models import Category, User, Topic, Reply
from services import users_service, topics_service, categories_service, replies_service
from common.responses import NotFound, Unauthorized
from routers import categories, topics, replies
from services.topics_service import remove_private_topics


class Replies_Should(unittest.TestCase):

    @patch('routers.replies.replies_service')
    @patch('routers.replies.topics_service')
    @patch('routers.replies.categories_service')
    def test_allReplies_returnsCorrectlyWhenNotLoggedInUserAndPrivateCategory(self, mock_categories, mock_topics, mock_replies):
        mock_topics.get_by_id.return_value = Topic(id=1, title='TestTopic1', text='text', user_id=1, category_id=1, created_on=None, replies=[1,2], locked = False, best_reply_id=0)
        
        mock_replies.all.return_value = [
        Reply(id=1, text='reply1',topic_id=1, user_id=1, created_on=None, votes=0),
        Reply(id=2, text='reply2',topic_id=1, user_id=2, created_on=None, votes=0)
        ]
       
        mock_categories.get_by_id.return_value = Category(id=1, name='TestCat1', locked = False, private = True, user_id=10)

        result = replies.get_replies(1, None)
        expected_type = Unauthorized

        self.assertIsInstance(result, expected_type)

    @patch('routers.replies.replies_service')
    @patch('routers.replies.topics_service')
    @patch('routers.replies.categories_service')
    @patch('routers.replies.users_service')
    def test_allReplies_returnsCorrectlyWhenAdmin(self, mock_users, mock_categories, mock_topics, mock_replies):
        mock_topics.get_by_id.return_value = Topic(id=1, title='TestTopic1', text='text', user_id=1, category_id=1, created_on=None, replies=[1,2], locked = False, best_reply_id=0)
        
        mock_replies.all.return_value = [
        Reply(id=1, text='reply1',topic_id=1, user_id=1, created_on=None, votes=0),
        Reply(id=2, text='reply2',topic_id=1, user_id=2, created_on=None, votes=0)
        ]
       
        mock_categories.get_by_id.return_value = Category(id=1, name='TestCat1', locked = False, private = True, user_id=10)

        mock_users.from_token.return_value = 'fake_credentials' 
        mock_users.find_user.return_value = User(id=1, username='john', password='john1', role='admin')
        
        result = replies.get_replies(1,'fake_token')
        expected = [
        Reply(id=1, text='reply1',topic_id=1, user_id=1, created_on=None, votes=0),
        Reply(id=2, text='reply2',topic_id=1, user_id=2, created_on=None, votes=0)
        ]

        self.assertEqual(result, expected)

    #==============================================

    @patch('routers.replies.replies_service')
    @patch('routers.replies.topics_service')
    @patch('routers.replies.categories_service')
    @patch('routers.replies.users_service')
    def test_Reply_returnsCorrectlyWhenLoggedInUserAndHasAccessToPrivate(self, mock_users, mock_categories, mock_topics, mock_replies):
        mock_topics.get_by_id.return_value = Topic(id=1, title='TestTopic1', text='text', user_id=1, category_id=1, created_on=None, replies=[1,2], locked = False, best_reply_id=0)
        mock_replies.get_by_id.return_value = Reply(id=1, text='reply1',topic_id=1, user_id=1, created_on=None, votes=0)
        mock_categories.get_by_id.return_value = Category(id=1, name='TestCat1', locked = False, private = True, user_id=10)

        mock_users.from_token.return_value = 'fake_credentials' 
        mock_users.find_user.return_value = User(id=1, username='john', password='john1', role='user')
        
        mock_categories.see_read_access.return_value = [5, 10, 1]
        
        result = replies.get_reply_by_id(1,'fake_token')
        expected = Reply(id=1, text='reply1',topic_id=1, user_id=1, created_on=None, votes=0)
        
        self.assertEqual(result, expected)

    #==============================================

    @patch('routers.replies.replies_service')
    @patch('routers.replies.users_service')
    @patch('routers.replies.topics_service')
    def test_CreateReply_returnsCorrectlyWhenAdmin(self, mock_topics, mock_users, mock_replies):  
        mock_users.from_token.return_value = 'fake_credentials' 
        mock_users.find_user.return_value = User(id=1, username='john', password='john1', role='admin')
        mock_topics.exists.return_value = True
        mock_topics.get_by_id.return_value = Topic(id=1, title='TestTopic1', text='text', user_id=1, category_id=1, created_on=None, replies=[1,2], locked = False, best_reply_id=0)
        mock_replies.create.return_value = Reply(id=1, text='reply1',topic_id=1, user_id=1, created_on=None, votes=0)

        result = replies.create_reply(Reply(id=1, text='reply1',topic_id=1, user_id=1, created_on=None, votes=0), 'fake_token')
        expected = Reply(id=1, text='reply1',topic_id=1, user_id=1, created_on=None, votes=0)

        self.assertEqual(result, expected)


    @patch('routers.replies.replies_service')
    @patch('routers.replies.users_service')
    @patch('routers.replies.topics_service')
    @patch('routers.replies.categories_service')
    def test_CreateReply_returnsCorrectlyWhenLoggedInUserAndHasAccessToPrivate(self,mock_categories, mock_topics, mock_users, mock_replies):  
        mock_users.from_token.return_value = 'fake_credentials' 
        mock_users.find_user.return_value = User(id=1, username='john', password='john1', role='user')
        mock_topics.exists.return_value = True
        mock_topics.get_by_id.return_value = Topic(id=1, title='TestTopic1', text='text', user_id=1, category_id=1, created_on=None, replies=[1,2], locked = False, best_reply_id=0)
        mock_replies.create.return_value = Reply(id=1, text='reply1',topic_id=1, user_id=1, created_on=None, votes=0)
        mock_categories.get_by_id.return_value = Category(id=1, name='TestCat1', locked = False, private = True, user_id=10)
        mock_categories.see_full_access.return_value = [5, 10, 1]
        
        result = replies.create_reply(Reply(id=1, text='reply1',topic_id=1, user_id=1, created_on=None, votes=0), 'fake_token')
        expected = Reply(id=1, text='reply1',topic_id=1, user_id=1, created_on=None, votes=0)

        self.assertEqual(result, expected)
    
    #==============================================

    @patch('routers.replies.replies_service')
    @patch('routers.replies.topics_service')
    @patch('routers.replies.categories_service')
    @patch('routers.replies.users_service')
    def test_UpvoteReply_returnsDataCorrectlyWhenLoggedInUserAndCategoryPrivate(self, mock_users, mock_categories, mock_topics, mock_replies):
        mock_replies.get_by_id.return_value = Reply(id=1, text='reply1',topic_id=1, user_id=1, created_on=None, votes=0)
        
        mock_users.from_token.return_value = 'fake_credentials' 
        mock_users.find_user.return_value = User(id=1, username='john', password='john1', role='user')
        
        mock_topics.get_by_id.return_value = Topic(id=1, title='TestTopic1', text='text', user_id=1, category_id=1, created_on=None, replies=[1], locked = False, best_reply_id=0)
        mock_categories.get_by_id.return_value = Category(id=1, name='TestCat1', locked = False, private = True, user_id=10)
        mock_categories.see_full_access.return_value = [5, 10, 2]

        result = replies.upvote_reply(1, 'fake_token')
        expected_type = Unauthorized

        self.assertIsInstance(result, expected_type)
    

    
    @patch('routers.replies.replies_service')
    @patch('routers.replies.topics_service')
    @patch('routers.replies.categories_service')
    @patch('routers.replies.users_service')
    def test_UpvoteReply_returnsDataCorrectly(self, mock_users, mock_categories, mock_topics, mock_replies):
        mock_replies.get_by_id.return_value = Reply(id=1, text='reply1',topic_id=1, user_id=1, created_on=None, votes=0)
        
        mock_users.from_token.return_value = 'fake_credentials' 
        mock_users.find_user.return_value = User(id=1, username='john', password='john1', role='user')
        
        mock_topics.get_by_id.return_value = Topic(id=1, title='TestTopic1', text='text', user_id=1, category_id=1, created_on=None, replies=[1], locked = False, best_reply_id=0)
        mock_categories.get_by_id.return_value = Category(id=1, name='TestCat1', locked = False, private = False, user_id=10)
        mock_replies.has_voted_up.return_value = False
        mock_replies.has_voted_down.return_value = False
        mock_replies.upvote.return_value = Reply(id=1, text='reply1',topic_id=1, user_id=1, created_on=None, votes=1)

        result = replies.upvote_reply(1, 'fake_token')
        expected = Reply(id=1, text='reply1',topic_id=1, user_id=1, created_on=None, votes=1)

        self.assertEqual(result, expected)
    

    @patch('routers.replies.replies_service')
    def test_UpvoteReply_returnsDataCorrectlyWhenNoSuchReply(self, mock_replies):
        mock_replies.get_by_id.return_value = None

        result = replies.upvote_reply(1, 'fake_token')
        expected_type = NotFound

        self.assertIsInstance(result, expected_type)

    #==============================================

    @patch('routers.replies.replies_service')
    @patch('routers.replies.topics_service')
    @patch('routers.replies.categories_service')
    @patch('routers.replies.users_service')
    def test_DownvoteReply_returnsDataCorrectlyWhenLoggedInUserHasVotedDown(self, mock_users, mock_categories, mock_topics, mock_replies):
        mock_replies.get_by_id.return_value = Reply(id=1, text='reply1',topic_id=1, user_id=1, created_on=None, votes=0)
        
        mock_users.from_token.return_value = 'fake_credentials' 
        mock_users.find_user.return_value = User(id=1, username='john', password='john1', role='user')
        
        mock_topics.get_by_id.return_value = Topic(id=1, title='TestTopic1', text='text', user_id=1, category_id=1, created_on=None, replies=[1,2], locked = False, best_reply_id=0)
        mock_categories.get_by_id.return_value = Category(id=1, name='TestCat1', locked = False, private = False, user_id=10)
        mock_replies.has_voted_down.return_value = True
        
        result = replies.upvote_reply(1, 'fake_token')
        expected_type = Unauthorized

        self.assertIsInstance(result, expected_type)
    
    @patch('routers.replies.replies_service')
    @patch('routers.replies.topics_service')
    @patch('routers.replies.categories_service')
    @patch('routers.replies.users_service')
    def test_DownvoteReply_returnsDataCorrectlyWhenLoggedInUserHasVotedUp(self, mock_users, mock_categories, mock_topics, mock_replies):
        mock_replies.get_by_id.return_value = Reply(id=1, text='reply1',topic_id=1, user_id=1, created_on=None, votes=1)
        
        mock_users.from_token.return_value = 'fake_credentials' 
        mock_users.find_user.return_value = User(id=1, username='john', password='john1', role='user')
        
        mock_topics.get_by_id.return_value = Topic(id=1, title='TestTopic1', text='text', user_id=1, category_id=1, created_on=None, replies=[1,2], locked = False, best_reply_id=0)
        mock_categories.get_by_id.return_value = Category(id=1, name='TestCat1', locked = False, private = False, user_id=10)
        mock_replies.has_voted_down.return_value = False
        mock_replies.has_voted_up.return_value = True
        mock_replies.downvote2.return_value = Reply(id=1, text='reply1',topic_id=1, user_id=1, created_on=None, votes=-1)
        
        result = replies.downvote_reply(1, 'fake_token')
        expected = Reply(id=1, text='reply1',topic_id=1, user_id=1, created_on=None, votes=-1)

        self.assertEqual(result, expected)
