from unittest.mock import patch
import unittest
from services import users_service
from data.models import User, LogInfo


class UsersService_Should(unittest.TestCase):
    
    TEST_USERS = [(1, 'john', 'john1', 'admin'), (2, 'alice', 'alice2', 'user')]
    

    @patch('services.users_service._hash')
    @patch('services.users_service.database')
    def test_findUser_returnsCorrectly(self, mock_database, mock_hash):
        mock_hash.return_value = 'alice2'
        mock_database.read_query.return_value = [(2, 'alice', 'alice2', 'user')]
        log_info = LogInfo(username='alice', password='alice2')

        result = users_service.find_user(log_info)
        expected = User(id=2, username='alice', password='alice2', role='user')
           
        self.assertEqual(result, expected)
    

    @patch('services.users_service._hash')
    @patch('services.users_service.database')
    def test_findUser_returnsCorrectly_whenNoSuchUser(self, mock_database, mock_hash):
        mock_hash.return_value = 'alice2'
        mock_database.read_query.return_value = None
        log_info = LogInfo(username='alice', password='alice2')

        result = users_service.find_user(log_info)
        expected = None
           
        self.assertEqual(result, expected)
    

    @patch('services.users_service.database')
    def test_all_retursnCorrectly(self, mock_database):
        mock_database.read_query.return_value = UsersService_Should.TEST_USERS

        result = users_service.all()
        expected = [User(id=u[0], username=u[1], password=u[2], role=u[3]) for u in UsersService_Should.TEST_USERS]

        self.assertEqual(result, expected)
    

    @patch('services.users_service.database')
    def test_all_returnsCorrectly_whenNoUsers(self, mock_database):
        mock_database.read_query.return_value = None

        result = users_service.all()
        expected = None

        self.assertEqual(result, expected)
    

    @patch('services.users_service._hash')
    @patch('services.users_service.database')
    def test_register_createsUser(self, mock_database, mock_hash):
        u = LogInfo(username='kevin', password='kevin3')
        mock_hash.return_value = u.password
        mock_database.insert_query.return_value = 3

        result = users_service.register(u)
        expected = User(id=3, username=u.username, password=u.password, role='user')

        self.assertEqual(result, expected)
    

    @patch('services.users_service.database')
    def test_findUserByUsername_returnsCorrectly(self, mock_database):
        mock_database.read_query.return_value = [(2, 'alice', 'alice2', 'user')]

        log_info = LogInfo(username='alice', password='alice2')

        result = users_service.find_user(log_info)
        expected = User(id=2, username='alice', password='alice2', role='user')
           
        self.assertEqual(result, expected)
    

    @patch('services.users_service.database')
    def test_findUserByUsername_returnsNone(self, mock_database):
        mock_database.read_query.return_value = None

        log_info = LogInfo(username='alice', password='alice2')

        result = users_service.find_user(log_info)
        expected = None
           
        self.assertEqual(result, expected)
    

    @patch('services.users_service.database')
    def test_makeAdmin_returnsCorrectly(self, mock_database):
        mock_database.update_query.return_value = True
        test_user = User(id=2, username='alice', password='alice2', role='user')

        result = users_service.make_admin(test_user)
        expected = User(id=2, username='alice', password='alice2', role='admin')

        self.assertEqual(result, expected)

