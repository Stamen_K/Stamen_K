from unittest.mock import patch
import unittest
from services import conversations_service
from data.models import User, LogInfo, Conversation


class ConversationsService_Should(unittest.TestCase):

    TEST_USERS = [('john', 'current'), ('alice', 'current'), ('current', 'alice')]
    TEST_CONVOS = [('john', 'text', 'current'), ('alice', 'text', 'current'), ('current', 'text', 'alice')]
    

    @patch('services.conversations_service.database')
    def test_all_returnsCorrectly(self, mock_database):
        mock_database.read_query.return_value = ConversationsService_Should.TEST_USERS
        test_user = User(id=1, username='current', password='meow', role='user')

        result = conversations_service.all(test_user)
        expected = {'john', 'alice'}

        self.assertEqual(result, expected)
    

    @patch('services.conversations_service.database')
    def test_all_returnsCorrectly_whenSelfMessaging(self, mock_database):
        mock_database.read_query.return_value = [('current', 'current'), ('current', 'current'), ('current', 'current')]
        test_user = User(id=1, username='current', password='meow', role='user')

        result = conversations_service.all(test_user)
        expected_len = 0

        self.assertEqual(len(result), expected_len)
    

    @patch('services.conversations_service.database')
    def test_all_returnsCorrectly_whenNoMessages(self, mock_database):
        mock_database.read_query.return_value = None
        test_user = User(id=1, username='current', password='meow', role='user')

        result = conversations_service.all(test_user)
        expected = []

        self.assertEqual(result, expected)
    

    @patch('services.conversations_service.database')
    def test_getConvoByUser_returnsCorrectly(self, mock_database):
        mock_database.read_query.return_value = [('alice', 'text', 'current'), ('current', 'text', 'alice')]
        test_logInfo = LogInfo(username='alice', password='alice')
        test_search = User(id=1, username='current', password='meow', role='user')

        result = conversations_service.get_convo_by_user(test_logInfo, test_search)
        expected = [Conversation(sender='alice', text='text', receiver='current'), Conversation(sender='current', text='text', receiver='alice')]

        self.assertEqual(result, expected)
    

    @patch('services.conversations_service.database')
    def test_getConvoByUser_returnsCorrectly_whenNoConvos(self, mock_database):
        mock_database.read_query.return_value = []
        test_logInfo = LogInfo(username='alice', password='alice')
        test_search = User(id=1, username='current', password='meow', role='user')

        result = conversations_service.get_convo_by_user(test_logInfo, test_search)
        expected = []

        self.assertEqual(result, expected)
    

    @patch('services.conversations_service.database')
    def test_message_returnsCorrectly(self, mock_database):
        mock_database.inser_query.return_value = 1
        test_sender = User(id=1, username='current', password='meow', role='user')
        test_reciever = User(id=2, username='john', password='meow', role='user')
        
        result = conversations_service.message(test_sender, 'text', test_reciever)
        expected = Conversation(sender='current', text='text', receiver='john')

        self.assertEqual(result, expected)

