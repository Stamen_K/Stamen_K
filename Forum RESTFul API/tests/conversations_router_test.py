import re
from unittest.mock import patch
import unittest
from common.responses import NotFound, Unauthorized
from data.models import Conversation, Message, User
from routers import conversations


class Conversations_Should(unittest.TestCase):

    
    @patch('routers.conversations.users_service')
    @patch('routers.conversations.conversations_service')
    def test_allConversations_returnsCorrectly(self, mock_conversations, mock_users):
        mock_users.from_token.return_value = 'fake_credentials'
        mock_users.find_user.return_value = User(id=1, username='john', password='john1', role='user')
        mock_conversations.all.return_value = ["alice", "admin"]

        result = conversations.all_conversations("fake_token")
        expected = ["alice", "admin"]

        self.assertEqual(result, expected)
    

    @patch('routers.conversations.users_service')
    def test_allConversations_fails_whenWrongToken(self, mock_users):
        mock_users.from_token.return_value = None

        result = conversations.all_conversations("fake_token")
        expected_type = Unauthorized

        self.assertIsInstance(result, expected_type)
    

    @patch('routers.conversations.users_service')
    @patch('routers.conversations.conversations_service')
    def test_getByUsername_returnsCorrectly(self, mock_conversations, mock_users):
        mock_users.find_user_by_username.return_value = User(id=1, username='john', password='john1', role='user')
        mock_users.from_token.return_value = 'fake_credentials'
        mock_conversations.get_convo_by_user.return_value = "fake_conversation"

        result = conversations.get_by_username("alice", "fake_token")
        expected = "fake_conversation"

        self.assertEqual(result, expected) 
    

    @patch('routers.conversations.users_service')
    def test_getByUsername_fails_whenWrongToken(self, mock_users):
        mock_users.find_user_by_username.return_value = User(id=1, username='john', password='john1', role='user')
        mock_users.from_token.return_value = None

        result = conversations.get_by_username("alice", "fake_token")
        expected_type = Unauthorized

        self.assertIsInstance(result, expected_type)
    

    @patch('routers.conversations.users_service')
    def test_getByUsername_fails_whenNoSuchUser(self, mock_users):
        mock_users.find_user_by_username.return_value = None

        result = conversations.get_by_username("alice", "fake_token")
        expected_type = NotFound

        self.assertIsInstance(result, expected_type)
    

    @patch('routers.conversations.users_service')
    @patch('routers.conversations.conversations_service')
    def test_message_returnsCorrectly(self, mock_conversations, mock_users):
        mock_users.from_token.return_value = "fake_token"
        mock_users.find_user_by_username.return_value = User(id=1, username='john', password='john1', role='user')
        mock_users.find_user.return_value = User(id=2, username='alice', password='alice2', role='admin')
        mock_conversations.message.return_value = Conversation(sender='alice', text="text", receiver="john")

        result = conversations.message("john", Message(text="text"), "fake_token")
        expected = Conversation(sender='alice', text="text", receiver="john")

        self.assertEqual(result, expected)
    

    @patch('routers.conversations.users_service')
    def test_message_fails_whenWrongToken(self, mock_users):
        mock_users.from_token.return_value = None

        result = conversations.message("john", Message(text="text"), "fake_token")
        expected_type = Unauthorized

        self.assertIsInstance(result, expected_type)
    

    @patch('routers.conversations.users_service')
    def test_message_fails_whenNoSuchUser(self, mock_users):
        mock_users.from_token.return_value = "fake_token"
        mock_users.find_user_by_username.return_value = None
        mock_users.find_user.return_value = User(id=2, username='alice', password='alice2', role='admin')

        result = conversations.message("john", Message(text="text"), "fake_token")
        expected_type = NotFound

        self.assertIsInstance(result, expected_type)