
from unittest import mock
from unittest.mock import patch
import unittest
from data.models import Category, CategoryResponseModel, CategoryUpdatePrivate, CategoryUpdateLocked, Privilliged_Users, User
from services import users_service, topics_service, categories_service
from common.responses import NotFound, Unauthorized
from routers import categories
from services.categories_service import sort, create_response_object, admin_lock_unlock


class Categories_Should(unittest.TestCase):

    @patch('routers.categories.categories_service')
    def test_allCategories_returnsCorrectlyWhenNotLoggedInUser(self, mock_categories):
        mock_categories.all.return_value = [
        Category(id=1, name='TestCat1', locked = False, private = False, user_id=10),
        Category(id=2, name='TestCat2', locked = False, private = True, user_id=11),
        Category(id=3, name='TestCat3', locked = True, private = False, user_id=12)
        ]

        result = categories.get_categories(None, None, None, None)
        expected = [
        Category(id=1, name='TestCat1', locked = False, private = False, user_id=10),
        Category(id=3, name='TestCat3', locked = True, private = False, user_id=12)
        ]

        self.assertEqual(result, expected)

    @patch('routers.categories.users_service')
    @patch('routers.categories.categories_service')
    def test_allCategories_returnsCorrectlyWhenLoggedInUser(self, mock_categories, mock_users):

        mock_categories.all.return_value = [
        Category(id=1, name='TestCat1', locked = False, private = False, user_id=10),
        Category(id=2, name='TestCat2', locked = False, private = True, user_id=11),
        Category(id=3, name='TestCat3', locked = True, private = True, user_id=12)
        ]

        mock_users.from_token.return_value = 'fake_credentials'
        mock_users.find_user.return_value = User(id=1, username='john', password='john1', role='user')

        mock_categories.nonprivate_categories_by_user.return_value = [
        Category(id=1, name='TestCat1', locked = False, private = False, user_id=10),
        Category(id=2, name='TestCat2', locked = False, private = True, user_id=11)
        ]

        result = categories.get_categories(None, None, None, 'fake token')
        expected = [
        Category(id=1, name='TestCat1', locked = False, private = False, user_id=10),
        Category(id=2, name='TestCat2', locked = False, private = True, user_id=11)
        ]

        self.assertEqual(result, expected)
    
    @patch('routers.categories.users_service')
    @patch('routers.categories.categories_service')
    def test_allCategories_returnsCorrectlyWhenUserIsAdmin(self, mock_categories, mock_users):

        mock_categories.all.return_value = [
        Category(id=1, name='TestCat1', locked = False, private = False, user_id=10),
        Category(id=2, name='TestCat2', locked = False, private = True, user_id=11),
        Category(id=3, name='TestCat3', locked = True, private = True, user_id=12)
        ]

        mock_users.from_token.return_value = 'fake_credentials'
        mock_users.find_user.return_value = User(id=1, username='john', password='john1', role='admin')
        
        result = categories.get_categories(None, None, None, 'fake token')
        expected = [
        Category(id=1, name='TestCat1', locked = False, private = False, user_id=10),
        Category(id=2, name='TestCat2', locked = False, private = True, user_id=11),
        Category(id=3, name='TestCat3', locked = True, private = True, user_id=12)
        ]
        self.assertEqual(result, expected)
    
    @patch('routers.categories.categories_service')
    def test_allCategories_returnsCorrectlyWhenNotLoggedInUserAndSortDesc(self, mock_categories):
        mock_categories.all.return_value = [
        Category(id=1, name='TestCat1', locked = False, private = False, user_id=10),
        Category(id=2, name='TestCat2', locked = False, private = True, user_id=11),
        Category(id=3, name='TestCat3', locked = True, private = False, user_id=12)]
        mock_categories.sort = sort

        result = categories.get_categories('desc','name', None, None)
        expected = [
        Category(id=3, name='TestCat3', locked = True, private = False, user_id=12),
        Category(id=1, name='TestCat1', locked = False, private = False, user_id=10)
        ]

        self.assertEqual(result, expected)
    

    @patch('routers.categories.categories_service')
    def test_allCategories_returnsCorrectlyWhenNoCategories(self, mock_categories):
        mock_categories.all.return_value = []

        result = categories.get_categories(None, None, None, None)
        expected = []

        self.assertEqual(result, expected)

#==============================================

    @patch('routers.categories.categories_service')
    @patch('routers.categories.topics_service')
    def test_Category_returnsCorrectlyWhenNotLoggedInUser(self, mock_topics, mock_categories):
        mock_categories.get_by_id.return_value = Category(id=1, name='TestCat1', locked = False, private = False, user_id=10)
        mock_topics.get_by_category.return_value = [1, 2, 3]
        mock_categories.create_response_object = create_response_object

        result = categories.get_category_by_id(1, None)
        expected = CategoryResponseModel(id=1, name='TestCat1', locked = False, private = False, user_id=10, topics = [1, 2, 3])

        self.assertEqual(result, expected)



    @patch('routers.categories.categories_service')
    @patch('routers.categories.users_service')
    def test_Category_returnsCorrectlyWhenLoggedInUserAndNoAccessToPrivate(self, mock_users, mock_categories):
        mock_categories.get_by_id.return_value = Category(id=1, name='TestCat1', locked = False, private = True, user_id=10)
        mock_users.from_token.return_value = 'fake_credentials' 
        mock_users.find_user.return_value = User(id=1, username='john', password='john1', role='user')
        mock_categories.see_read_access.return_value = [5, 10]

        result = categories.get_category_by_id(1,'fake_token')
        expected_type = Unauthorized

        self.assertIsInstance(result, expected_type)


    @patch('routers.categories.categories_service')
    @patch('routers.categories.users_service')
    @patch('routers.categories.topics_service')
    def test_Category_returnsCorrectlyWhenLoggedInUserAndHasAccessToPrivate(self, mock_topics, mock_users, mock_categories):
        mock_categories.get_by_id.return_value = Category(id=1, name='TestCat1', locked = False, private = True, user_id=10)
        mock_users.from_token.return_value = 'fake_credentials' 
        mock_users.find_user.return_value = User(id=1, username='john', password='john1', role='user')
        mock_categories.see_read_access.return_value = [5, 10, 1]
        mock_topics.get_by_category.return_value = [1, 2, 3]
        mock_categories.create_response_object = create_response_object 

        result = categories.get_category_by_id(1,'fake_token')
        expected = CategoryResponseModel(id=1, name='TestCat1', locked = False, private = True, user_id=10, topics = [1, 2, 3])


        self.assertEqual(result, expected)


    @patch('routers.categories.categories_service')
    def test_Category_returnsCorrectlyWhenNoSuchCategory(self, mock_categories):
        mock_categories.get_by_id.return_value = None

        result = categories.get_category_by_id(1, None)
        expected_type = NotFound

        self.assertIsInstance(result, expected_type)


    @patch('routers.categories.categories_service')
    @patch('routers.categories.users_service')
    def test_Category_returnsCorrectlyWhenNotLoggedInUser(self, mock_users, mock_categories):
        mock_categories.get_by_id.return_value = Category(id=1, name='TestCat1', locked = False, private = True, user_id=10)
        mock_users.from_token.return_value = None

        result = categories.get_category_by_id(1, None)
        expected_type = Unauthorized

        self.assertIsInstance(result, expected_type)

    
#==========================================


    @patch('routers.categories.users_service')
    def test_CreateCategory_returnsCorrectlyWhenWrongToken(self, mock_users):
        mock_users.from_token.return_value = None

        result = categories.create_category(Category(id=1, name='TestCat1', locked = False, private = False), 'wrong_token')
        expected_type = Unauthorized

        self.assertIsInstance(result, expected_type)


    @patch('routers.categories.users_service')
    def test_CreateCategory_returnsCorrectlyWhenLoggedInUser(self, mock_users):
        mock_users.from_token.return_value = 'fake_credentials'
        mock_users.find_user.return_value = User(id=1, username='john', password='john1', role='user')

        result = categories.create_category(Category(id=1, name='TestCat1', locked = False, private = False),'fake_token')
        expected_type = Unauthorized

        self.assertIsInstance(result, expected_type)


    @patch('routers.categories.users_service')
    @patch('routers.categories.categories_service')
    def test_CreateCategory_returnsCorrectlyWhenLoggedIsAdmin(self, mock_categories, mock_users):
        mock_users.from_token.return_value = 'fake_credentials'
        mock_users.find_user.return_value = User(id=1, username='john', password='john1', role='admin')
        mock_categories.create.return_value = Category(id=1, name='TestCat1', locked = False, private = False)

        result = categories.create_category(Category(id=1, name='TestCat1', locked = False, private = False),'fake_token')
        expected = Category(id=1, name='TestCat1', locked = False, private = False)

        self.assertEqual(result, expected)

#==========================================

    @patch('routers.categories.users_service')
    def test_CategoryLockUnlock_returnsCorrectlyWhenWrongToken(self, mock_users):
        mock_users.from_token.return_value = None

        result = categories.category_lock_unlock(Category(id=1, name='TestCat1', locked = False, private = False), 'wrong_token')
        expected_type = Unauthorized

        self.assertIsInstance(result, expected_type)


    @patch('routers.categories.users_service')
    @patch('routers.categories.categories_service')
    def test_CategoryLockUnlock_returnsCorrectlyWhenCategoryNotFound(self, mock_categories, mock_users):
        mock_users.from_token.return_value = 'fake_credentials'
        mock_users.find_user.return_value = User(id=1, username='john', password='john1', role='admin')
        mock_categories.get_by_id.return_value = None

        result = categories.category_lock_unlock(Category(id=1, name='TestCat1', locked = False, private = False), 'fake_token')
        expected_type = NotFound

        self.assertIsInstance(result, expected_type)


    @patch('routers.categories.users_service')
    @patch('routers.categories.categories_service')
    def test_CategoryLockUnlock_returnsCorrectlyWhenAdmin(self, mock_categories, mock_users):
        mock_users.from_token.return_value = 'fake_credentials'
        mock_users.find_user.return_value = User(id=1, username='john', password='john1', role='admin')
        mock_categories.get_by_id.return_value = Category(id=1, name='TestCat1', locked = False, private = False)
        mock_categories.admin_lock_unlock.return_value = Category(id=1, name='TestCat1', locked = True, private = False)

        result = categories.category_lock_unlock(Category(id=1, name='TestCat1', locked = False, private = False), 'fake_token')
        expected = Category(id=1, name='TestCat1', locked = True, private = False)

        self.assertEqual(result, expected)


    @patch('routers.categories.users_service')
    @patch('routers.categories.categories_service')
    def test_CategoryLockUnlock_returnsCorrectlyWhenUser(self, mock_categories, mock_users):
        mock_users.from_token.return_value = 'fake_credentials'
        mock_users.find_user.return_value = User(id=1, username='john', password='john1', role='user')
        mock_categories.get_by_id.return_value = Category(id=1, name='TestCat1', locked = False, private = False)


        result = categories.category_lock_unlock(Category(id=1, name='TestCat1', locked = False, private = False), 'fake_token')
        expected_type = Unauthorized

        self.assertIsInstance(result, expected_type)

#==================================

    @patch('routers.categories.users_service')
    def test_CategoryPrivateOrNot_returnsCorrectlyWhenWrongToken(self, mock_users):
        mock_users.from_token.return_value = None

        result = categories.category_private_or_not(Category(id=1, name='TestCat1', locked = False, private = False), 'wrong_token')
        expected_type = Unauthorized

        self.assertIsInstance(result, expected_type)


    @patch('routers.categories.users_service')
    @patch('routers.categories.categories_service')
    def test_CategoryPrivateOrNot_returnsCorrectlyWhenCategoryNotFound(self, mock_categories, mock_users):
        mock_users.from_token.return_value = 'fake_credentials'
        mock_users.find_user.return_value = User(id=1, username='john', password='john1', role='admin')
        mock_categories.get_by_id.return_value = None

        result = categories.category_private_or_not(Category(id=1, name='TestCat1', locked = False, private = False), 'fake_token')
        expected_type = NotFound

        self.assertIsInstance(result, expected_type)


    @patch('routers.categories.users_service')
    @patch('routers.categories.categories_service')
    def test_CategoryPrivateOrNot_returnsCorrectlyWhenAdmin(self, mock_categories, mock_users):
        mock_users.from_token.return_value = 'fake_credentials'
        mock_users.find_user.return_value = User(id=1, username='john', password='john1', role='admin')
        mock_categories.get_by_id.return_value = Category(id=1, name='TestCat1', locked = False, private = False)
        mock_categories.admin_private_or_not.return_value = Category(id=1, name='TestCat1', locked = False, private = True)

        result = categories.category_private_or_not(Category(id=1, name='TestCat1', locked = False, private = False), 'fake_token')
        expected = Category(id=1, name='TestCat1', locked = False, private = True)

        self.assertEqual(result, expected)


    @patch('routers.categories.users_service')
    @patch('routers.categories.categories_service')
    def test_CategoryPrivateOrNot_returnsCorrectlyWhenUser(self, mock_categories, mock_users):
        mock_users.from_token.return_value = 'fake_credentials'
        mock_users.find_user.return_value = User(id=1, username='john', password='john1', role='user')
        mock_categories.get_by_id.return_value = Category(id=1, name='TestCat1', locked = False, private = False)


        result = categories.category_private_or_not(Category(id=1, name='TestCat1', locked = False, private = False), 'fake_token')
        expected_type = Unauthorized

        self.assertIsInstance(result, expected_type)

#=================================


    @patch('routers.categories.users_service')
    def test_CategoryAccess_returnsCorrectlyWhenWrongToken(self, mock_users):
        mock_users.from_token.return_value = None

        result = categories.add_private_users(Privilliged_Users(cat_id = 1 , speical_users = [2, 3]), 'wrong_token')
        expected_type = Unauthorized

        self.assertIsInstance(result, expected_type)


    @patch('routers.categories.users_service')
    @patch('routers.categories.categories_service')
    def test_CategoryAccess_returnsCorrectlyWhenCategoryNotFound(self, mock_categories, mock_users):
        mock_users.from_token.return_value = 'fake_credentials'
        mock_users.find_user.return_value = User(id=1, username='john', password='john1', role='admin')
        mock_categories.get_by_id.return_value = None

        result = categories.add_private_users(Privilliged_Users(cat_id = 1 , speical_users = [2, 3]), 'fake_token')
        expected_type = NotFound

        self.assertIsInstance(result, expected_type)


    @patch('routers.categories.users_service')
    @patch('routers.categories.categories_service')
    def test_CategoryAccess_returnsCorrectlyWhenUser(self, mock_categories, mock_users):
        mock_users.from_token.return_value = 'fake_credentials'
        mock_users.find_user.return_value = User(id=1, username='john', password='john1', role='user')
        mock_categories.get_by_id.return_value = Category(id=1, name='TestCat1', locked = False, private = False)


        result = categories.add_private_users(Privilliged_Users(cat_id = 1 , speical_users = [2, 3]), 'fake_token')
        expected_type = Unauthorized

        self.assertIsInstance(result, expected_type)


    @patch('routers.categories.users_service')
    @patch('routers.categories.categories_service')
    def test_CategoryAccess_returnsCorrectlyWhenAdmin(self, mock_categories, mock_users):
        mock_users.from_token.return_value = 'fake_credentials'
        mock_users.find_user.return_value = User(id=1, username='john', password='john1', role='admin')
        mock_categories.get_by_id.return_value = Category(id=1, name='TestCat1', locked = False, private = True)
        mock_users.validate_users.return_value = True
        mock_categories.add_users_to_private.return_value = [2, 3]

        result = categories.add_private_users(Privilliged_Users(cat_id = 1 , speical_users = [2, 3]), 'fake_token')
        expected = [2, 3]

        self.assertEqual(result, expected)