
from unittest import mock
from unittest.mock import patch
import unittest
from data.models import Category, Reply, TopicResponseModel, User, Topic
from services import users_service, topics_service, categories_service
from common.responses import NotFound, Unauthorized
from routers import categories, topics
from services.topics_service import create_response_object
from datetime import datetime


class Topics_Should(unittest.TestCase):

    @patch('routers.topics.topics_service')
    @patch('routers.topics.categories_service')
    def test_allTopics_returnsCorrectlyWhenNotLoggedInUserAndCategoryPrivate(self, mock_categories, mock_topics):
        mock_topics.all.return_value = [
        Topic(id=1, title='TestTopic1', text='text', user_id=1, category_id=1, created_on=None, replies=[1,2,3], locked = False, best_reply_id=0),
        Topic(id=2, title='TestTopic2', text='text', user_id=1, category_id=2, created_on=None, replies=[], locked = False, best_reply_id=0)
        ]
        mock_categories.all.return_value = [
            Category(id=1, name='TestCat1', locked = False, private = False, user_id=10),
            Category(id=2, name='TestCat2', locked = False, private = True, user_id=11)
            ]
        mock_topics.remove_private_topics.return_value = Topic(
            id=1, title='TestTopic1', text='text', user_id=1, category_id=1,
            created_on=None, replies=[1,2,3], locked = False, best_reply_id=0)

        result = topics.get_topics(None, None, None)
        expected = Topic(id=1, title='TestTopic1', text='text', user_id=1, category_id=1, created_on=None, replies=[1,2,3], locked = False, best_reply_id=0)

        self.assertEqual(result, expected)

    @patch('routers.topics.topics_service')
    @patch('routers.topics.categories_service')
    def test_allTopics_returnsCorrectlyWhenNotLoggedInUserAndNoPrivateCategory(self, mock_categories, mock_topics):
        mock_topics.all.return_value = [
        Topic(id=1, title='TestTopic1', text='text', user_id=1, category_id=1, created_on=None, replies=[1,2,3], locked = False, best_reply_id=0),
        Topic(id=2, title='TestTopic2', text='text', user_id=1, category_id=2, created_on=None, replies=[], locked = False, best_reply_id=0)
        ]
        mock_categories.all.return_value = [
            Category(id=1, name='TestCat1', locked = False, private = False, user_id=10),
            Category(id=2, name='TestCat2', locked = False, private = False, user_id=11)
            ]
        mock_topics.remove_private_topics.return_value = [
            Topic(id=1, title='TestTopic1', text='text', user_id=1, category_id=1, created_on=None, replies=[1,2,3], locked = False, best_reply_id=0),
            Topic(id=2, title='TestTopic2', text='text', user_id=1, category_id=2, created_on=None, replies=[], locked = False, best_reply_id=0)
        ]
        result = topics.get_topics(None, None, None)
        expected = [
            Topic(id=1, title='TestTopic1', text='text', user_id=1, category_id=1, created_on=None, replies=[1,2,3], locked = False, best_reply_id=0),
            Topic(id=2, title='TestTopic2', text='text', user_id=1, category_id=2, created_on=None, replies=[], locked = False, best_reply_id=0)
        ]
        self.assertEqual(result, expected)
        
    #==============================================

    @patch('routers.topics.topics_service')
    @patch('routers.topics.categories_service')
    def test_Topic_returnsCorrectlyWhenNotLoggedInUserAndCategoryIsNotPrivate(self, mock_categories, mock_topics):
        mock_topics.get_by_id.return_value = Topic(id=1, title='TestTopic1', text='text', user_id=1, category_id=1, created_on='2022-10-10', replies=[1,2], locked = False, best_reply_id=0)
        mock_categories.get_by_id.return_value = Category(id=1, name='TestCat1', locked = False, private = False, user_id=10)
        mock_topics.get_topic_replies.return_value = [
            Reply(id=1, text='reply1', topic_id=1, user_id=1, created_on='2022-10-10', votes=0),
            Reply(id=2, text='reply2', topic_id=1, user_id=2, created_on='2022-10-10', votes=0)
        ]
        mock_topics.create_response_object = create_response_object
        
        result = topics.get_topic_by_id(1, None)
        expected = TopicResponseModel(id=1, title='TestTopic1', text='text', user_id=1, category_id=1, created_on='2022-10-10', replies=[
            Reply(id=1, text='reply1', topic_id=1, user_id=1, created_on='2022-10-10', votes=0),
            Reply(id=2, text='reply2', topic_id=1, user_id=2, created_on='2022-10-10', votes=0)], locked = False, best_reply_id=0)
        
        self.assertEqual(result, expected)

    @patch('routers.topics.topics_service')
    @patch('routers.topics.categories_service')
    @patch('routers.topics.users_service')
    def test_Topic_returnsCorrectlyWhenLoggedInUserAndHasAccessToPrivate(self, mock_users, mock_categories, mock_topics):
        mock_topics.get_by_id.return_value = Topic(id=1, title='TestTopic1', text='text', user_id=1, category_id=1, created_on='2022-10-10', replies=[1,2], locked = False, best_reply_id=0)
        mock_categories.get_by_id.return_value = Category(id=1, name='TestCat1', locked = False, private = True, user_id=10)
        mock_users.from_token.return_value = 'fake_credentials'
        mock_users.find_user.return_value = User(id=1, username='john', password='john1', role='user')
        mock_categories.see_read_access.return_value = [5, 10, 1]
        mock_topics.get_topic_replies.return_value = [
            Reply(id=1, text='reply1', topic_id=1, user_id=1, created_on='2022-10-10', votes=0),
            Reply(id=2, text='reply2', topic_id=1, user_id=2, created_on='2022-10-10', votes=0)
        ]
        mock_topics.create_response_object = create_response_object
        
        result = topics.get_topic_by_id(1, 'fake_token')
        expected = TopicResponseModel(id=1, title='TestTopic1', text='text', user_id=1, category_id=1, created_on='2022-10-10', replies=[
            Reply(id=1, text='reply1', topic_id=1, user_id=1, created_on='2022-10-10', votes=0),
            Reply(id=2, text='reply2', topic_id=1, user_id=2, created_on='2022-10-10', votes=0)], locked = False, best_reply_id=0)
        
        self.assertEqual(result, expected)

    #==============================================

    @patch('routers.topics.categories_service')
    @patch('routers.topics.users_service')
    def test_CreateTopic_returnsCorrectlyWhenNotAdminAndLockedTopic(self, mock_users, mock_categories):
        mock_users.from_token.return_value = 'fake_credentials'
        mock_users.find_user.return_value = User(id=1, username='john', password='john1', role='user')
        mock_categories.get_by_id.return_value = Category(id=1, name='TestCat1', locked = False, private = False, user_id=10)

        result = topics.create_topic(Topic(id=1, title='TestTopic1', text='text', user_id=1, category_id=1, created_on=None, replies=[], locked = True, best_reply_id=0),'fake_token')
        expected_type = Unauthorized

        self.assertIsInstance(result, expected_type)
    
    @patch('routers.topics.categories_service')
    @patch('routers.topics.users_service')
    @patch('routers.topics.topics_service')
    def test_CreateTopic_returnsCorrectlyWhenLoggedInUser(self,mock_topics, mock_users, mock_categories):
        mock_users.from_token.return_value = 'fake_credentials'
        mock_users.find_user.return_value = User(id=1, username='john', password='john1', role='user')
       
        mock_categories.get_by_id.return_value = Category(id=1, name='TestCat1', locked = False, private = False, user_id=10)
        mock_topics.create.return_value = Topic(id=1, title='TestTopic1', text='text', user_id=1, category_id=1, created_on=datetime.utcnow(), replies=[], locked = True, best_reply_id=0)

        result = topics.create_topic(Topic(id=1, title='TestTopic1', text='text', user_id=1, category_id=1, created_on=datetime.utcnow(), replies=[], locked = False, best_reply_id=0),'fake_token')
        expected = Topic(id=1, title='TestTopic1', text='text', user_id=1, category_id=1, created_on=datetime.utcnow(), replies=[], locked = True, best_reply_id=0)
        self.assertEqual(result, expected)

    #==============================================

    @patch('routers.topics.users_service')
    @patch('routers.topics.topics_service')
    def test_UpdateTopic_returnsDataCorrectlyWhenAdmin(self, mock_topics, mock_users):
        mock_users.from_token.return_value = 'fake_credentials'
        mock_users.find_user.return_value = User(id=1, username='john', password='john1', role='admin')
        mock_topics.get_by_id.return_value = Topic(id=1, title='TestTopic1', text='text', user_id=1, category_id=1, created_on=None, replies=[1,2], locked = False, best_reply_id=0)
        mock_topics.update.return_value = Topic(id=1, title='TestTopic1', text='text', user_id=1, category_id=1, created_on=None, replies=[1,2,3], locked = False, best_reply_id=0)
        
        result = topics.update_topic(Topic(id=1, title='TestTopic1', text='text', user_id=1, category_id=1, created_on=None, replies=[1,2,3], locked = False, best_reply_id=0), 'fake_token')
        expected = Topic(id=1, title='TestTopic1', text='text', user_id=1, category_id=1, created_on=None, replies=[1,2,3], locked = False, best_reply_id=0)

        self.assertEqual(result, expected)