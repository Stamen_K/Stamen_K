-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema forum_db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema forum_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `forum_db` DEFAULT CHARACTER SET utf8mb3 ;
USE `forum_db` ;

-- -----------------------------------------------------
-- Table `forum_db`.`categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum_db`.`categories` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `locked` TINYINT(1) NOT NULL DEFAULT 0,
  `private` TINYINT(1) NOT NULL DEFAULT 0,
  `user_id` INT(200) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `forum_db`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum_db`.`users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) CHARACTER SET 'latin1' COLLATE 'latin1_bin' NOT NULL,
  `password` VARCHAR(100) CHARACTER SET 'latin1' COLLATE 'latin1_bin' NOT NULL,
  `role` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `forum_db`.`conversations`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum_db`.`conversations` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `sender` INT(11) NOT NULL,
  `text` TINYTEXT NOT NULL,
  `receiver` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_messages_users1_idx` (`sender` ASC) VISIBLE,
  INDEX `fk_messages_users2_idx` (`receiver` ASC) VISIBLE,
  CONSTRAINT `fk_messages_users1`
    FOREIGN KEY (`sender`)
    REFERENCES `forum_db`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_messages_users2`
    FOREIGN KEY (`receiver`)
    REFERENCES `forum_db`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `forum_db`.`topics`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum_db`.`topics` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `text` LONGTEXT NULL DEFAULT NULL,
  `categories_id` INT(11) NOT NULL,
  `users_id` INT(11) NOT NULL,
  `locked` TINYINT(4) NOT NULL,
  `created` DATETIME NOT NULL,
  `best_reply` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_topics_categories_idx` (`categories_id` ASC) VISIBLE,
  INDEX `fk_topics_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_topics_categories`
    FOREIGN KEY (`categories_id`)
    REFERENCES `forum_db`.`categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_topics_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `forum_db`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `forum_db`.`replies`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum_db`.`replies` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `text` MEDIUMTEXT NULL DEFAULT NULL,
  `topics_id` INT(11) NOT NULL,
  `users_id` INT(11) NOT NULL,
  `upvotes` INT(11) NOT NULL DEFAULT 0,
  `created` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_replies_topics1_idx` (`topics_id` ASC) VISIBLE,
  INDEX `fk_replies_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_replies_topics1`
    FOREIGN KEY (`topics_id`)
    REFERENCES `forum_db`.`topics` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_replies_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `forum_db`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `forum_db`.`users_access`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum_db`.`users_access` (
  `users_id` INT(11) NOT NULL,
  `categories_id` INT(11) NOT NULL,
  `access_level` INT(11) NOT NULL,
  PRIMARY KEY (`users_id`, `categories_id`),
  INDEX `fk_bans_users1_idx` (`users_id` ASC) VISIBLE,
  INDEX `fk_bans_categories1_idx` (`categories_id` ASC) VISIBLE,
  CONSTRAINT `fk_bans_categories1`
    FOREIGN KEY (`categories_id`)
    REFERENCES `forum_db`.`categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_bans_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `forum_db`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `forum_db`.`votes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum_db`.`votes` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `score` INT(11) NOT NULL,
  `users_id` INT(11) NOT NULL,
  `replies_id` INT(11) NOT NULL,
  `vote` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_upvotes_users1_idx` (`users_id` ASC) VISIBLE,
  INDEX `fk_upvotes_replies1_idx` (`replies_id` ASC) VISIBLE,
  CONSTRAINT `fk_upvotes_replies1`
    FOREIGN KEY (`replies_id`)
    REFERENCES `forum_db`.`replies` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_upvotes_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `forum_db`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
