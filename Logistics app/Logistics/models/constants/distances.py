
from models.constants.locations import Locations


def add_route_kilometers(self):
        for k in range (len(self.locations)-1):    
            if self.locations[k] == Locations.SYDNEY:
                if self.locations[k+1] == Locations.MELBOURNE:
                    distance = 877
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.ADELAIDE:
                    distance = 1376
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.ALICE_SPRINGS:
                    distance = 2762
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.BRISBANE:
                    distance = 909
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.DARWIN:
                    distance = 3935
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.PERTH:
                    distance = 4016
                    self._route_kilometers.append(distance)
            if self.locations[k] == Locations.MELBOURNE:
                if self.locations[k+1] == Locations.SYDNEY:
                    distance = 877
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.ADELAIDE:
                    distance = 725
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.ALICE_SPRINGS:
                    distance = 2255
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.BRISBANE:
                    distance = 1765
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.DARWIN:
                    distance = 3752
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.PERTH:
                    distance = 3509
                    self._route_kilometers.append(distance)
            if self.locations[k] == Locations.ADELAIDE:
                if self.locations[k+1] == Locations.SYDNEY:
                    distance = 1376
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.MELBOURNE:
                    distance = 725
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.ALICE_SPRINGS:
                    distance = 1530
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.BRISBANE:
                    distance = 1927
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.DARWIN:
                    distance = 3027
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.PERTH:
                    distance = 2785
                    self._route_kilometers.append(distance)
            if self.locations[k] == Locations.ALICE_SPRINGS:
                if self.locations[k+1] == Locations.SYDNEY:
                    distance = 2762
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.MELBOURNE:
                    distance = 2255
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.ADELAIDE:
                    distance = 1530
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.BRISBANE:
                    distance = 2993
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.DARWIN:
                    distance = 1497
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.PERTH:
                    distance = 2481
                    self._route_kilometers.append(distance)
            if self.locations[k] == Locations.BRISBANE:
                if self.locations[k+1] == Locations.SYDNEY:
                    distance = 909
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.MELBOURNE:
                    distance = 1765
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.ADELAIDE:
                    distance = 1927
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.ALICE_SPRINGS:
                    distance = 2993
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.DARWIN:
                    distance = 3426
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.PERTH:
                    distance = 4311
                    self._route_kilometers.append(distance)
            if self.locations[k] == Locations.DARWIN:
                if self.locations[k+1] == Locations.SYDNEY:
                    distance = 3935
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.MELBOURNE:
                    distance = 3752
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.ADELAIDE:
                    distance = 3027
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.ALICE_SPRINGS:
                    distance = 1497
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.BRISBANE:
                    distance = 3426
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.PERTH:
                    distance = 4025
                    self._route_kilometers.append(distance)
            if self.locations[k] == Locations.PERTH:
                if self.locations[k+1] == Locations.SYDNEY:
                    distance = 4016
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.MELBOURNE:
                    distance = 3509
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.ADELAIDE:
                    distance = 2785
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.ALICE_SPRINGS:
                    distance = 2481
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.BRISBANE:
                    distance = 4311
                    self._route_kilometers.append(distance)
                if self.locations[k+1] == Locations.DARWIN:
                    distance = 4025
                    self._route_kilometers.append(distance)