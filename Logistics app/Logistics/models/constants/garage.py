from models.truck import Truck


class Garage:
    Unusable_truck = Truck('TruckUnassigned', 'TruckUnassigned', 0, 10000)
    Scania_1 = Truck(1001, 'Scania', 42000, 8000)
    Scania_2 = Truck(1002, 'Scania', 42000, 8000)
    Scania_3 = Truck(1003, 'Scania', 42000, 8000)
    Scania_4 = Truck(1004, 'Scania', 42000, 8000)
    Scania_5 = Truck(1005, 'Scania', 42000, 8000)
    Scania_6 = Truck(1006, 'Scania', 42000, 8000)
    Scania_7 = Truck(1007, 'Scania', 42000, 8000)
    Scania_8 = Truck(1008, 'Scania', 42000, 8000)
    Scania_9 = Truck(1009, 'Scania', 42000, 8000)
    Scania_10 = Truck(1010, 'Scania', 42000, 8000)
    Man_1 = Truck(1011, 'Man', 37000, 10000)
    Man_2 = Truck(1012, 'Man', 37000, 10000)
    Man_3 = Truck(1013, 'Man', 37000, 10000)
    Man_4 = Truck(1014, 'Man', 37000, 10000)
    Man_5 = Truck(1015, 'Man', 37000, 10000)
    Man_6 = Truck(1016, 'Man', 37000, 10000)
    Man_7 = Truck(1017, 'Man', 37000, 10000)
    Man_8 = Truck(1018, 'Man', 37000, 10000)
    Man_9 = Truck(1019, 'Man', 37000, 10000)
    Man_10 = Truck(1020, 'Man', 37000, 10000)
    Man_11 = Truck(1021, 'Man', 37000, 10000)
    Man_12 = Truck(1022, 'Man', 37000, 10000)
    Man_13 = Truck(1023, 'Man', 37000, 10000)
    Man_14 = Truck(1024, 'Man', 37000, 10000)
    Man_15 = Truck(1025, 'Man', 37000, 10000)
    Actros_1 = Truck(1026, 'Actros', 26000, 13000)
    Actros_2 = Truck(1027, 'Actros', 26000, 13000)
    Actros_3 = Truck(1028, 'Actros', 26000, 13000)
    Actros_4 = Truck(1029, 'Actros', 26000, 13000)
    Actros_5 = Truck(1030, 'Actros', 26000, 13000)
    Actros_6 = Truck(1031, 'Actros', 26000, 13000)
    Actros_7 = Truck(1032, 'Actros', 26000, 13000)
    Actros_8 = Truck(1033, 'Actros', 26000, 13000)
    Actros_9 = Truck(1034, 'Actros', 26000, 13000)
    Actros_10 = Truck(1035, 'Actros', 26000, 13000)
    Actros_11 = Truck(1036, 'Actros', 26000, 13000)
    Actros_12 = Truck(1037, 'Actros', 26000, 13000)
    Actros_13 = Truck(1038, 'Actros', 26000, 13000)
    Actros_14 = Truck(1039, 'Actros', 26000, 13000)
    Actros_15 = Truck(1040, 'Actros', 26000, 13000)

    def __init__(self):
        self._vehicles = [Garage.Scania_1, Garage.Scania_2, Garage.Scania_3, Garage.Scania_4, Garage.Scania_5, Garage.Scania_6, Garage.Scania_7, Garage.Scania_8, Garage.Scania_9, Garage.Scania_10,
        Garage.Man_1, Garage.Man_2, Garage.Man_3, Garage.Man_4, Garage.Man_5, Garage.Man_6, Garage.Man_7, Garage.Man_8, Garage.Man_9, Garage.Man_10, Garage.Man_11, Garage.Man_12, Garage.Man_13, Garage.Man_14, Garage.Man_15,
        Garage.Actros_1, Garage.Actros_2, Garage.Actros_3, Garage.Actros_4, Garage.Actros_5, Garage.Actros_6, Garage.Actros_7, Garage.Actros_8, Garage.Actros_9, Garage.Actros_10, Garage.Actros_11, Garage.Actros_12,
        Garage.Actros_13, Garage.Actros_14, Garage.Actros_15]
    
    @property
    def total_vehicles(self):
        return tuple(self._vehicles)
    
    @property
    def available_vehicles(self):
        return self._vehicles