class Locations:
    PERTH = 'Perth'
    ALICE_SPRINGS = 'Alice_Springs'
    DARWIN = 'Darwin'
    ADELAIDE = 'Adelaide'
    MELBOURNE = 'Melbourne'
    SYDNEY = 'Sydney'
    BRISBANE = 'Brisbane'

    @property
    def locations(self):
        return (Locations.ADELAIDE, Locations.ALICE_SPRINGS, Locations.BRISBANE, Locations.DARWIN, Locations.MELBOURNE, Locations.PERTH, Locations.SYDNEY)

