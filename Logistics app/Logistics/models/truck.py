from models.package import Package

class Truck:

    def __init__(self, id, name, capacity, max_range):
        self._id = id
        self._name = name
        self._capacity = capacity
        self._max_range = max_range
        self._trunk: list [Package] = []
    
    
    @property
    def id(self):
        return self._id

    @property
    def name(self):
        return self._name

    @property
    def capacity(self):
        return self._capacity
    
    @property
    def max_range(self):
        return self._max_range
    
    @property
    def trunk(self):
        return self._trunk
    
    def load_package(self, package: Package):
        if package.weight > self.capacity:
            raise ValueError('Not enough capacity.')
        self._capacity -= package.weight
        self._trunk.append(package)
        
    def __str__(self) -> str:
        trunk_items = []
        for package in self._trunk:
            trunk_items.append(f'  #{package.id} {str(package)}')
        trunk_items = '\n'.join(trunk_items)

        expected = [f'   Truck number: {self.id}, remaining capacity: {self.capacity}']
        if len(self._trunk) == 0:
            expected.append(' (empty)')
        else:
            expected.append(trunk_items)
        expected = '\n'.join(expected)

        return expected
    

