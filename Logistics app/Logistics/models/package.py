from models.constants.locations import Locations
from models.date_creator import DateCreator
from validation_helpers.compare_dates import isbigger_date


class Package:
    ID = 1
    LOCATIONS = Locations()
    LOCATIONS = LOCATIONS.locations
    
    def __init__(self, start_loc: str, end_loc: str, weight: int, contact_info: str, submission: DateCreator, deadline: DateCreator):
        self._id = Package.ID
        
        if start_loc not in Package.LOCATIONS:
            raise ValueError('Nonexisting location.')
        self._start_loc = start_loc
        
        if end_loc not in Package.LOCATIONS:
            raise ValueError('Nonexisting location.')
        self._end_loc = end_loc
        
        if weight < 0:
            raise ValueError('Negative weight')
        self._weight = weight
        
        self.contact_info = contact_info
        
        if not isbigger_date(deadline, submission):
            raise ValueError('Package cant arrive before it departs')
        self._submission = submission
        self._deadline = deadline

        Package.ID += 1
    
    @property
    def start_loc(self):
        return self._start_loc
    
    @property
    def end_loc(self):
        return self._end_loc
    
    @property
    def contact_info(self):
        return self._contact_info
    
    @contact_info.setter
    def contact_info(self, value):
        if '@gmail.com' not in value:
            raise ValueError('Must use gmail account')
        self._contact_info = value

    @property
    def id(self):
        return self._id
    
    @property
    def weight(self):
        return self._weight
    
    @property
    def submission(self):
        return self._submission
    
    @property
    def deadline(self):
        return self._deadline
    
    def info(self):
        expected = [f'Package {self.id}:', f' from: {self.start_loc}, to: {self.end_loc}', f' weight: {self.weight}', f' sender: {self.contact_info}', 
        f' sumbmitted: {self.submission} deadline: {self.deadline}']
        expected = '\n'.join(expected)
        return expected
    
    def __str__(self) -> str:
        return f'Package weight: {self._weight}kg'