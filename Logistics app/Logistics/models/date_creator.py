class DateCreator:
    def __init__(self, time: str, date: str):
        self._date = date.split('.')
        self._time = time.split(':')
        
        self._day = int(self._date[0])
        if self._day > 31 or self._day < 0:
            raise ValueError('Invalid Date')
        
        self._month = int(self._date[1])
        if self._month > 12 or self._month < 0:
            raise ValueError('Invalid Date')

        self._year = int(self._date[2])
        
        self._hour = int(self._time[0])
        
        if self._time[1][0] == '0':
            self._time[1] = self._time[1][1]
        self._minute = int(self._time[1])
        
        if self._hour > 23 or self._hour < 0 or self._minute > 59 or self._minute < 0:
            raise ValueError('Invalid time')
    
    @property
    def day(self):
        return self._day
    
    @property
    def month(self):
        return self._month
    
    @property
    def year(self):
        return self._year
    
    @property
    def hour(self):
        return self._hour
    
    @property
    def minute(self):
        return self._minute
    
    def next_date(self, minutes):
        new_hour = self.hour
        new_minute = self.minute
        new_day = self.day
        new_month = self.month
        new_year = self.year

        new_minute += minutes
        if new_minute > 59:
            hours_to_add = new_minute//60
            new_minute = new_minute%60
            new_hour += hours_to_add
        if new_hour > 23:
            days_to_add = new_hour//24
            new_hour = new_hour%24
            new_day += days_to_add
        if new_day > 31:
            new_day = new_day - 31
            new_month += 1
        if new_month > 12:
            new_month = 1
            new_year += 1
        
        return DateCreator(f'{new_hour}:{new_minute}', f'{new_day}.{new_month}.{new_year}')

        
    def __str__(self) -> str:
        if self.minute < 10:
            stringified_minute = f'0{self.minute}'
            return f'{self.hour}:{stringified_minute} {self.day}.{self.month}.{self.year}'
        return f'{self.hour}:{self.minute} {self.day}.{self.month}.{self.year}'

