from models.constants.garage import Garage
from models.constants.locations import Locations
from models.date_creator import DateCreator
from models.truck import Truck
from validation_helpers.compare_dates import isbigger_date, validate_dates
from validation_helpers.distances import add_route_kilometers


class Route:
    ID = 1
    LOCATIONS = Locations()
    LOCATIONS = LOCATIONS.locations
    Truck_speed = 87
    
    def __init__(self, locations: list [Locations], departure_time: str, departure_date: str):
        for element in locations:
            if element not in Route.LOCATIONS:
                raise ValueError('Non existing location')
        self._locations = locations
        
        self.id = Route.ID
        self._truck: list [Truck] = []
        self._route_kilometers: list [int] = []
        self._date_1 = DateCreator(departure_time, departure_date)
        add_route_kilometers(self)
        
        Route.ID += 1
    
    @property
    def truck(self):
        if len(self._truck) > 0:
            return self._truck[0]
        else:
            return Garage.Unusable_truck
    
    @property
    def locations(self):
        return tuple(self._locations)
    
    @property
    def route_kilometers(self):
        return self._route_kilometers
    
    @property
    def total_distance(self):
        return sum(self._route_kilometers)
    
    @property
    def dates(self):
        all_dates: list [DateCreator] = [self._date_1]
        for km in self.route_kilometers:
           new_date = all_dates[-1].next_date(int((km/Route.Truck_speed)*60))
           all_dates.append(new_date)
        return all_dates

    def add_truck(self, value: Truck):
        if len(self._truck) > 0:
            raise ValueError('Vehicle already added.')
        if value.max_range < sum(self._route_kilometers):
            raise ValueError ('This truck cannot complete the route.')
        self._truck.append(value)
    
    def info(self):
        stringified = [f' {self.locations[k]}({str(self.dates[k])})' for k in range(len(self.locations))]
        stringified = ' -> '.join(stringified)
        expected = [f'#Route {self.id}']
        expected.append(stringified)
        expected.append(f'{str(self.truck)}')
        expected = '\n'.join(expected)
        return expected
    
    def __str__(self) -> str:
        stringified = [f' {self.locations[k]}({str(self.dates[k])})' for k in range(len(self.locations))]
        stringified = ' -> '.join(stringified)
        expected = [f'#Route {self.id}']
        expected.append(stringified)
        expected.append(f'{str(self.truck)}')
        expected = '\n'.join(expected)
        return expected
    
    
    

            

