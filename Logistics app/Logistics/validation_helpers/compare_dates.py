from models.date_creator import DateCreator

def isbigger_date(date_1: DateCreator, date_2: DateCreator):
    if date_1.year > date_2.year:
        return True
    elif date_1.year == date_2.year and date_1.month > date_2.month:
        return True
    elif date_1.year == date_2.year and date_1.month == date_2.month:
        if date_1.day >= date_2.day:
            return True
    return False

def validate_dates(dates: list [DateCreator]):
    check = True
    for k in range(1, len(dates)):
        if isbigger_date(dates[k], dates[k-1]) ==  False:
            check = False
    return check