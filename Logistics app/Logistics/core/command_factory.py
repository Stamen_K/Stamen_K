from commands.add_package import AddPackage
from commands.create_package_command import CreatePackageCommand
from commands.find_suitable_routes import FindSuitableRoutes
from commands.add_package import  AddPackage
from commands.add_truck_command import AddTruckCommand
from commands.create_route_command import CreateRouteCommand
from commands.view_package_command import ViewPackageCommand
from commands.view_route_command import ViewRouteCommand
from core.application_data import ApplicationData


class CommandFactory:
    def __init__(self, data: ApplicationData):
        self._app_data = data

    def create(self, input_line):
        cmd, *params = input_line.split()

        if cmd.lower() == "createroute":
            return CreateRouteCommand(params, self._app_data)
        if cmd.lower() == 'viewroute':
            return ViewRouteCommand(params, self._app_data)
        if cmd.lower() == 'addtruck':
            return AddTruckCommand(params, self._app_data)
        if cmd.lower() == 'addpackage':
            return AddPackage(params, self._app_data)
        if cmd.lower() == 'createpackage':
            return CreatePackageCommand(params, self._app_data)
        if cmd.lower() == 'viewpackage':
            return ViewPackageCommand(params, self._app_data)
        if cmd.lower() == 'findsuitableroutes':
            return FindSuitableRoutes(params, self._app_data)
        
        raise ValueError(f'Invalid command name: {cmd}!')
