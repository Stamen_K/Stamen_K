from models.constants.garage import Garage
from models.deliver_route import Route
from models.package import Package
from models.truck import Truck
from validation_helpers.compare_dates import isbigger_date


class ApplicationData:
    def __init__(self):
        self._routes: list[Route] = []
        self._packages: list[Package] = []
        garage = Garage()
        self._total_vehicles: tuple [Truck] = garage.total_vehicles
        self._available: list [Truck] = garage.available_vehicles
    
    @property
    def available(self):
        return tuple(self._available)
    
    @property
    def total_vehicles(self):
        return self._total_vehicles
    
    @property
    def routes(self):
        return tuple(self._routes)
    
    @property
    def packages(self):
        return tuple(self._packages)
    
    def check_vehicle(self, vehicleID: int):
        availabe_ids = [vehicle.id for vehicle in self.available]
        all_ids = [vehicle.id for vehicle in self.total_vehicles]
        
        if vehicleID in availabe_ids and vehicleID in all_ids:
            return True
        return False
    
    def remove_vehicle(self, vehicleID: int):
        for vehicle in self._available:
            if vehicle.id == vehicleID:
                self._available.remove(vehicle)

    def add_route(self, value: Route):
        self._routes.append(value)
    
    def create_package(self, value: Package):
        self._packages.append(value)

    def add_package(self, packageID, routeID):
        for element in self.packages:
            if element.id == packageID:
                package = element
        
        for route in self.routes:
            if route.id == routeID:
                if route.locations.index(package.start_loc) < route.locations.index(package.end_loc) and isbigger_date(route.dates[route.locations.index(package.start_loc)], package.submission) and isbigger_date(package.deadline, route.dates[route.locations.index(package.end_loc)]):
                    route.truck.load_package(package)
                    return True     
        raise ValueError('This route is not suitable')