import unittest

from models.date_creator import DateCreator


class DateCreatorShould(unittest.TestCase):
   
   def test_init_setsPropertiesCorrect(self):
        
        #Arrange and act
        time = '17:20'
        date = '26.7.2022'
        day, month, year = 26, 7, 2022
        hour, minute = 17, 20
        date_creator = DateCreator(time, date)

        #Assert
        self.assertEqual(date_creator.day, day)
        self.assertEqual(date_creator.month, month)
        self.assertEqual(date_creator.year, year)
        self.assertEqual(date_creator.hour, hour)
        self.assertEqual(date_creator.minute, minute)

   def test_init_setsPropertiesCorrect_whenMinutesUnderTen(self):
        
        #Arrange and act
        time = '17:02'
        date = '26.7.2022'
        day, month, year = 26, 7, 2022
        hour, minute = 17, 2
        date_creator = DateCreator(time, date)

        #Assert
        self.assertEqual(date_creator.day, day)
        self.assertEqual(date_creator.month, month)
        self.assertEqual(date_creator.year, year)
        self.assertEqual(date_creator.hour, hour)
        self.assertEqual(date_creator.minute, minute)

   def test_init_failsDayTooBig(self):
        
        #Arrange and act
        time = '17:20'
        date = '43.7.2022'
        
        #Assert
        with self.assertRaises(ValueError):
            DateCreator(time, date)
    
   def test_init_failsDayNegative(self):
        
        #Arrange and act
        time = '17:20'
        date = '-20.7.2022'
        
        #Assert
        with self.assertRaises(ValueError):
            DateCreator(time, date)
   
   def test_init_failsMonthTooBig(self):

        #Arrange and act
        time = '17:20'
        date = '26.14.2022'
        
        #Assert
        with self.assertRaises(ValueError):
            DateCreator(time, date)
    
   def test_init_failsMonthNegative(self):

        #Arrange and act
        time = '17:20'
        date = '26.-5.2022'
        
        #Assert
        with self.assertRaises(ValueError):
            DateCreator(time, date)

   def test_init_failsHoursTooBig(self):

        #Arrange and act
        time = '25:20'
        date = '26.7.2022'
        
        #Assert
        with self.assertRaises(ValueError):
            DateCreator(time, date)

   def test_init_failsHoursNegative(self):

        #Arrange and act
        time = '-12:20'
        date = '26.7.2022'
        
        #Assert
        with self.assertRaises(ValueError):
            DateCreator(time, date)

   def test_init_failsMinutesTooBig(self):

        #Arrange and act
        time = '17:76'
        date = '26.7.2022'
        
        #Assert
        with self.assertRaises(ValueError):
            DateCreator(time, date)

   def test_init_failsMinutesNegative(self):

        #Arrange and act
        time = '17:-15'
        date = '26.7.2022'
        
        #Assert
        with self.assertRaises(ValueError):
            DateCreator(time, date)\

   def test_init_failsHoursTooBig(self):

        #Arrange and act
        time = '25:20'
        date = '26.7.2022'
        
        #Assert
        with self.assertRaises(ValueError):
            DateCreator(time, date)

   def test_nextDate_changesMinutes(self):

        #Arrange and act
        time = '17:05'
        date = '26.7.2022'
        minutes = 25
        date_1 = DateCreator(time, date)
        
        date_2 = date_1.next_date(minutes)
        
        #Assert
        self.assertEqual(date_2.minute, 30)
        self.assertEqual(date_2.hour, date_1.hour)
        self.assertEqual(date_2.day, date_1.day)
        self.assertEqual(date_2.month, date_1.month)
        self.assertEqual(date_2.year, date_1.year)

   def test_nextDate_changesHours(self):

        #Arrange and act
        time = '17:20'
        date = '26.7.2022'
        minutes = 60
        date_1 = DateCreator(time, date)
        
        date_2 = date_1.next_date(minutes)
        
        #Assert
        self.assertEqual(date_2.hour, 18)
        self.assertEqual(date_2.minute, 20)
        self.assertEqual(date_2.day, date_1.day)
        self.assertEqual(date_2.month, date_1.month)
        self.assertEqual(date_2.year, date_1.year)

   def test_nextDate_changesDay(self):

        #Arrange and act
        time = '23:50'
        date = '26.7.2022'
        minutes = 20
        date_1 = DateCreator(time, date)
        
        date_2 = date_1.next_date(minutes)
        
        #Assert
        self.assertEqual(date_2.minute, 10)
        self.assertEqual(date_2.hour, 0)
        self.assertEqual(date_2.day, 27)
        self.assertEqual(date_2.month, date_1.month)
        self.assertEqual(date_2.year, date_1.year)

   def test_nextDate_changesMonth(self):

        #Arrange and act
        time = '23:50'
        date = '31.7.2022'
        minutes = 20
        date_1 = DateCreator(time, date)
        
        date_2 = date_1.next_date(minutes)
        
        #Assert
        self.assertEqual(date_2.minute, 10)
        self.assertEqual(date_2.hour, 0)
        self.assertEqual(date_2.day, 1)
        self.assertEqual(date_2.month, 8)
        self.assertEqual(date_2.year, date_1.year)

   def test_nextDate_changesYear(self):

        #Arrange and act
        time = '23:50'
        date = '31.12.2022'
        minutes = 20
        date_1 = DateCreator(time, date)
        
        date_2 = date_1.next_date(minutes)
        
        #Assert
        self.assertEqual(date_2.minute, 10)
        self.assertEqual(date_2.hour, 0)
        self.assertEqual(date_2.day, 1)
        self.assertEqual(date_2.month, 1)
        self.assertEqual(date_2.year, 2023)

   def test_str_returnsCorrect(self):

        #Arrange and act
        time = '23:50'
        date = '31.12.2022'
        date_1 = DateCreator(time, date)
        stringified = f'23:50 31.12.2022'
        
        #Assert
        self.assertEqual(str(date_1), stringified)

   def test_str_returnsCorrect_whenMinutesUnderTen(self):

        #Arrange and act
        time = '23:05'
        date = '31.12.2022'
        date_1 = DateCreator(time, date)
        stringified = f'23:05 31.12.2022'
        
        #Assert
        self.assertEqual(str(date_1), stringified)
    




    