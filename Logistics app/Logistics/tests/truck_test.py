import unittest
from models.date_creator import DateCreator
from models.package import Package

from models.truck import Truck

VALID_ID = 1003
VALID_NAME = 'Man'
VALID_CAPACITY = 37000
VALID_MAX_RANGE = 10000

class Truck_Should(unittest.TestCase):

    def test_init_setsPropertiesCorrectly(self):

        #Arrange & act
        truck1 = Truck(VALID_ID, VALID_NAME, VALID_CAPACITY, VALID_MAX_RANGE)
        
        #Assert
        self.assertEqual(VALID_ID, truck1.id)
        self.assertEqual(VALID_NAME, truck1.name)
        self.assertEqual(VALID_CAPACITY, truck1.capacity)
        self.assertEqual(VALID_MAX_RANGE, truck1.max_range)
        self.assertEqual([], truck1.trunk)
    
    def test_load_package_AddsOnePackageToTrunk(self):

        #Arrange
        start_loc, end_loc, weight, customer_info = 'Sydney', 'Melbourne', 9000, 'pesho@gmail.com'
        submission  = DateCreator('0:00', '12.10.2022')
        deadline = DateCreator('23:59', '15.10.2022')
        truck1 = Truck(VALID_ID, VALID_NAME, VALID_CAPACITY, VALID_MAX_RANGE)
        package1 = Package(start_loc, end_loc, weight, customer_info, submission, deadline)

        #Act
        truck1.load_package(package1)

        #Assert
        self.assertEqual(1, len(truck1.trunk))
        self.assertEqual(package1, truck1.trunk[0])

    def test_load_package_AddsMorePackagesToTrunk(self):
        
        #Arrange
        start_loc, end_loc, weight, customer_info = 'Sydney', 'Melbourne', 9000, 'pesho@gmail.com'
        submission  = DateCreator('0:00', '12.10.2022')
        deadline = DateCreator('23:59', '15.10.2022')
        truck1 = Truck(VALID_ID, VALID_NAME, VALID_CAPACITY, VALID_MAX_RANGE)
        package1 = Package(start_loc, end_loc, weight, customer_info, submission, deadline)
        package2 = Package(start_loc, end_loc, weight, customer_info, submission, deadline)

        #Act
        truck1.load_package(package1)
        truck1.load_package(package2)

        #Assert
        self.assertEqual(2, len(truck1.trunk))
        self.assertEqual(package1, truck1.trunk[0])
        self.assertEqual(package2, truck1.trunk[1])

    def test_load_package_RaisesErrorWhenNotEnoughtCapacity(self):

        # Arrange
        start_loc, end_loc, weight, customer_info = 'Sydney', 'Melbourne', 40000, 'pesho@gmail.com'
        submission  = DateCreator('0:00', '12.10.2022')
        deadline = DateCreator('23:59', '15.10.2022')
        truck1 = Truck(VALID_ID, VALID_NAME, VALID_CAPACITY, VALID_MAX_RANGE)
        package1 = Package(start_loc, end_loc, weight, customer_info, submission, deadline)

        #  Act & Assert
        with self.assertRaises(ValueError):
            truck1.load_package(package1)

    def test_load_package_ReturnCorrectCapacityWhenPackageAdded(self):
        
        # Arrange
        start_loc, end_loc, weight, customer_info = 'Sydney', 'Melbourne', 1000, 'pesho@gmail.com'
        submission  = DateCreator('0:00', '12.10.2022')
        deadline = DateCreator('23:59', '15.10.2022')
        truck1 = Truck(VALID_ID, VALID_NAME, VALID_CAPACITY, VALID_MAX_RANGE)    
        package1 = Package(start_loc, end_loc, weight, customer_info, submission, deadline)

        #Act
        truck1.load_package(package1)
        #Assert
        self.assertEqual(36000, truck1._capacity)

    def test_str_returnsStringCorrectlyWhenEmptyTrunk(self):
        # Arrange
        truck1 = Truck(VALID_ID, VALID_NAME, VALID_CAPACITY, VALID_MAX_RANGE)
        
        expected = f'   Truck number: {truck1.id}, remaining capacity: {truck1.capacity}\n (empty)'

        # Act
        actual = f'{truck1}'

        # Assert
        self.assertEqual(expected, actual)
    
    def test_str_returnsStringCorrectlyWhenNotEmptyTrunk(self):
        # Arrange
        start_loc, end_loc, weight, customer_info = 'Sydney', 'Melbourne', 1000, 'pesho@gmail.com'
        submission  = DateCreator('0:00', '12.10.2022')
        deadline = DateCreator('23:59', '15.10.2022')
        test_truck = Truck(VALID_ID, VALID_NAME, VALID_CAPACITY, VALID_MAX_RANGE)    
        package2 = Package(start_loc, end_loc, weight, customer_info, submission, deadline)
        
        test_truck.load_package(package2)
        expected = f'   Truck number: {test_truck.id}, remaining capacity: {test_truck.capacity}\n  #6 Package weight: {package2.weight}kg'

        # Act
        actual = f'{test_truck}'

        # Assert
        self.assertEqual(expected, actual)
    
    