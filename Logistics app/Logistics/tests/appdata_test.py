import unittest

from core.application_data import ApplicationData
from models.constants.garage import Garage
from models.date_creator import DateCreator
from models.deliver_route import Route
from models.package import Package
from models.truck import Truck

class ApplicationData_Should(unittest.TestCase):

    def test_properties_returnCorrectTypes(self):
        data = ApplicationData()
        self.assertIsInstance(data.routes, tuple)
        self.assertIsInstance(data.packages, tuple)
        self.assertIsInstance(data.available, tuple)
        self.assertIsInstance(data.total_vehicles, tuple)
    
    def test_checkVehicle_ReturnsTrueWhenAvailableVehicle(self):
        # Arrange
        data = ApplicationData()
        vehicle = Garage.Actros_11
        
        # Act & Assert
        self.assertTrue(data.check_vehicle(vehicle.id))
    
    def test_removeVehicle_RemovesVehicleSuccessfully(self):
        # Arrange
        data = ApplicationData()
        vehicle = Garage.Actros_11
        
        # Act 
        data.remove_vehicle(vehicle.id)
        # Assert
        self.assertEqual(len(data.total_vehicles)-1, len(data._available))
    
    def test_checkVehicle_ReturnsFalseWhenNotAvailableVehicle(self):
        # Arrange
        data = ApplicationData()
        vehicle = Garage.Actros_11
        data.remove_vehicle(vehicle.id)
        
        # Act & Assert
        self.assertFalse(data.check_vehicle(vehicle.id))
        
    def test_add_route_addedRouteSuccessfully(self):
        # Arrange
        data = ApplicationData()
        route_1 = Route(['Sydney', 'Melbourne', 'Perth', 'Darwin'], '22:54', '12.08.2022')

        # Act
        data.add_route(route_1)

        # Assert
        self.assertEqual(1, len(data.routes))
        self.assertEqual(route_1, data.routes[0])
    
    def test_add_route_addedMoreRoutesSuccessfully(self):
        # Arrange
        data = ApplicationData()
        route_1 = Route(['Sydney', 'Melbourne', 'Perth', 'Darwin'], '22:54', '12.08.2022')
        route_2 = Route(['Sydney', 'Melbourne'], '10:10', '13.08.2022')

        # Act
        data.add_route(route_1)
        data.add_route(route_2)

        # Assert
        self.assertEqual(2, len(data.routes))
        self.assertEqual(route_1, data.routes[0])
        self.assertEqual(route_2, data.routes[1])
    
    def test_createPackage_createsPackageSuccessfully(self):
        # Arrange
        data = ApplicationData()
        submission = DateCreator('12:15','12.08.2022')
        deadline = DateCreator('23:59','16.08.2022')
        package_1 = Package('Adelaide', 'Brisbane', 1000, 'stamen@gmail.com', submission, deadline)

        # Act
        data.create_package(package_1)

        # Assert
        self.assertEqual(1, len(data.packages))
        self.assertEqual(package_1, data.packages[0])


    def test_createPackage_createsMorePackageSuccessfully(self):
        # Arrange
        data = ApplicationData()
        submission = DateCreator('12:15','12.08.2022')
        deadline = DateCreator('23:59','16.08.2022')
        package_1 = Package('Adelaide', 'Brisbane', 1000, 'stamen@gmail.com', submission, deadline)
        package_2 = Package('Sydney', 'Melbourne', 1000, 'stamen@gmail.com', submission, deadline)

        # Act
        data.create_package(package_1)
        data.create_package(package_2)

        # Assert
        self.assertEqual(2, len(data.packages))
        self.assertEqual(package_1, data.packages[0])
        self.assertEqual(package_2, data.packages[1])
    
    def test_addPackage_addsPackageToRouteSuccessfully(self):
        # Arrange
        data = ApplicationData()
        route_1 = Route(['Sydney', 'Melbourne', 'Perth', 'Darwin'], '22:54', '12.08.2022')
        data.add_route(route_1)
        truck = Truck(1001, 'Scania', 42000, 20000)
        route_1.add_truck(truck)
        submission = DateCreator('12:15','12.08.2022')
        deadline = DateCreator('23:59','26.08.2022')
        package_1 = Package('Melbourne', 'Perth', 1000, 'stamen@gmail.com', submission, deadline)
        data.create_package(package_1)
        route_1.truck.load_package(package_1)

        # Act
        data.add_package(1,1)

        # Assert
        self.assertTrue(data.add_package(1,1))
    
    def test_addPackage_addsPackageToRoute_raisesError_whenRouteIsNotSuitable(self):
        # Arrange
        data = ApplicationData()
        route_1 = Route(['Sydney', 'Melbourne', 'Perth', 'Darwin'], '22:54', '12.08.2022')
        data.add_route(route_1)
        truck = Truck(1001, 'Scania', 42000, 20000)
        route_1.add_truck(truck)
        submission = DateCreator('12:15','12.08.2022')
        deadline = DateCreator('23:59','26.08.2022')
        package_1 = Package('Melbourne', 'Sydney', 1000, 'stamen@gmail.com', submission, deadline)
        data.create_package(package_1)
        route_1.truck.load_package(package_1)

        # Assert
        with self.assertRaises(ValueError):  
            data.add_package(1,1)
    
    def test_addPackage_addsPackageToRoute_raisesError_whenRouteIsNotExisting(self):
        # Arrange
        data = ApplicationData()
        route_1 = Route(['Sydney', 'Melbourne', 'Perth', 'Darwin'], '22:54', '12.08.2022')
        data.add_route(route_1)
        truck = Truck(1001, 'Scania', 42000, 20000)
        route_1.add_truck(truck)
        submission = DateCreator('12:15','12.08.2022')
        deadline = DateCreator('23:59','26.08.2022')
        package_1 = Package('Melbourne', 'Sydney', 1000, 'stamen@gmail.com', submission, deadline)
        data.create_package(package_1)
        route_1.truck.load_package(package_1)

        # Assert
        with self.assertRaises(ValueError):  
            data.add_package(1,2)
    
    def test_addPackage_addsPackageToRoute_raisesError_whenPackageIsNotExisting(self):
        # Arrange
        data = ApplicationData()
        route_1 = Route(['Sydney', 'Melbourne', 'Perth', 'Darwin'], '22:54', '12.08.2022')
        data.add_route(route_1)
        truck = Truck(1001, 'Scania', 42000, 20000)
        route_1.add_truck(truck)
        submission = DateCreator('12:15','12.08.2022')
        deadline = DateCreator('23:59','26.08.2022')
        package_1 = Package('Melbourne', 'Sydney', 1000, 'stamen@gmail.com', submission, deadline)
        data.create_package(package_1)
        route_1.truck.load_package(package_1)

        # Assert
        with self.assertRaises(ValueError):  
            data.add_package(2,1)
    
   
        
    
    
        
        