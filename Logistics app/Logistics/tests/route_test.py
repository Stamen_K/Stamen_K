import unittest

from models.deliver_route import Route
from models.truck import Truck

VALID_LOCATION = ['Sydney', 'Melbourne','Perth']
VALID_TIME = '15:00'
VALID_DATE = '15.08.2022'
INVALID_LOCATION = ['Sofia', 'Melbourne','Perth']

class Route_Should(unittest.TestCase):
    
    def test_initializer_assignsValues(self):

        #Arrange & Act
        Route.ID = 1
        route = Route(VALID_LOCATION, VALID_TIME, VALID_DATE)
        #Assert
        self.assertEqual(1, route.id)
        self.assertEqual(['Sydney', 'Melbourne','Perth'], route._locations)
        self.assertEqual([877, 3509], route._route_kilometers)
        self.assertEqual([], route._truck)
        self.assertEqual('15:00 15.8.2022', str(route._date_1))

    def test_properties_returnCorrectTypes(self):
        #Arrange, Act, Assert
        route = Route(VALID_LOCATION, VALID_TIME, VALID_DATE)
        self.assertIsInstance(route.id, int)
        self.assertIsInstance(route.locations, tuple)
        self.assertIsInstance(route.route_kilometers, list)
        self.assertIsInstance(route.total_distance, int)
        self.assertIsInstance(route.truck, list)
        self.assertIsInstance(route.dates, list)
    
    def test_initializer_failsToSetProperties_startLocationIncorrect(self):

        #Arrange & Act & Assert
        with self.assertRaises(ValueError):
            route = Route(INVALID_LOCATION, VALID_TIME, VALID_DATE)
    
    def test_total_distance_ReturnCorrectSum(self):

        #Arrange & Act
        route = Route(VALID_LOCATION, VALID_TIME, VALID_DATE)

        #Assert
        self.assertEqual(4386, route.total_distance)

    
    def test_route_kilometers_ReturnDataForEveryDistance(self):
        #Arrange & Act
        route = Route(VALID_LOCATION, VALID_TIME, VALID_DATE)

        #Assert
        self.assertEqual(2, len(route._route_kilometers))

    def test_add_truck_AddsTruckSuccessfully(self):

        #Arrange
        route = Route(VALID_LOCATION, VALID_TIME, VALID_DATE)
        truck = Truck(1011, 'Man', 37000, 10000)
        #Act
        route.add_truck(truck)

        #Assert
        self.assertEqual(1, len(route._truck))
        self.assertEqual(truck, route._truck[0])

    def test_add_truck_FailWhenTruckAlreadyAdded(self):
        #Arrange & Act
        route = Route(VALID_LOCATION, VALID_TIME, VALID_DATE)
        truck = Truck(1011, 'Man', 37000, 10000)
        truck1 = Truck(1012, 'Man', 37000, 10000)
        route.add_truck(truck)

        #Assert
        with self.assertRaises(ValueError):
            route.add_truck(truck1)

    def test_add_truck_FailWhenTruckCannotCompleteRoute(self):

        #Arrange
        route = Route(VALID_LOCATION, VALID_TIME, VALID_DATE)
        truck = Truck(1011, 'Man', 37000, 1000)

        #Act & Assert
        with self.assertRaises(ValueError):
            route.add_truck(truck)
    
    def test_dates_AppendSuccessfullyForEveryLocation(self):

        #Arrange & Act
        route = Route(VALID_LOCATION, VALID_TIME, VALID_DATE)

        #Assert
        self.assertEqual(3, len(route.dates))
        self.assertEqual('15:00 15.8.2022', str(route.dates[0]))
        self.assertEqual('1:04 16.8.2022', str(route.dates[1]))
        self.assertEqual('17:24 17.8.2022', str(route.dates[2]))

    def test_str_returnsStringCorrectly(self):

        # Arrange
        route = Route(VALID_LOCATION, VALID_TIME, VALID_DATE)
        truck = Truck(1011, 'Man', 37000, 10000)
        route.add_truck(truck)
        expected = f'#Route {route.id}\n {route.locations[0]}({str(route.dates[0])}) ->  {route.locations[1]}({str(route.dates[1])}) ->  {route.locations[2]}({str(route.dates[2])})\n{str(truck)}'
       
        # Act
        actual = f'{route}'

        # Assert
        self.assertEqual(expected, actual)

    def test_info_returnsStringCorrectly(self):

        # Arrange
        route = Route(VALID_LOCATION, VALID_TIME, VALID_DATE)
        truck = Truck(1011, 'Man', 37000, 10000)
        route.add_truck(truck)
        expected = f'#Route {route.id}\n {route.locations[0]}({str(route.dates[0])}) ->  {route.locations[1]}({str(route.dates[1])}) ->  {route.locations[2]}({str(route.dates[2])})\n{str(truck)}'
       
        # Act
        actual = f'{route}'

        # Assert
        self.assertEqual(expected, actual)
        

    

    
