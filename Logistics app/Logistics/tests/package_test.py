import unittest
from models.date_creator import DateCreator

from models.package import Package


class PackageShould(unittest.TestCase):

    def test_init_setsPropertiesCorrectly(self):

        #Arrange and act
        s_loc, e_loc, weight, c_info = 'Sydney', 'Melbourne', 9000, 'pesho@gmail.com'
        submission  = DateCreator('0:00', '12.10.2022')
        deadline = DateCreator('23:59', '15.10.2022')
        Package.ID = 1
        package = Package(s_loc, e_loc, weight, c_info, submission, deadline)
        package_2 = Package(s_loc, e_loc, weight, c_info, submission, deadline)

        #Assert
        self.assertEqual(package.id, 1)
        self.assertEqual(package_2.id, 2)
        self.assertEqual(package.start_loc, s_loc)
        self.assertEqual(package.end_loc, e_loc)
        self.assertEqual(package.weight, weight)
        self.assertEqual(package.contact_info, c_info)
        self.assertEqual(str(package.submission), '0:00 12.10.2022')
        self.assertEqual(str(package.deadline), '23:59 15.10.2022')
    
    def test_init_failsToSetProperties_startLocationIncorrect(self):

        #Arrange and act
        s_loc, e_loc, weight, c_info = 'Pleven', 'Melbourne', 9000, 'pesho@gmail.com'
        submission  = DateCreator('0:00', '12.10.2022')
        deadline = DateCreator('23:59', '15.10.2022')
        
        #Assert
        with self.assertRaises(ValueError):
            Package(s_loc, e_loc, weight, c_info, submission, deadline)
    
    def test_init_failsToSetProperties_endLocationIncorrect(self):

        #Arrange and act
        s_loc, e_loc, weight, c_info = 'Sydney', 'Plovdiv', 9000, 'pesho@gmail.com'
        submission  = DateCreator('0:00', '12.10.2022')
        deadline = DateCreator('23:59', '15.10.2022')
        
        #Assert
        with self.assertRaises(ValueError):
            Package(s_loc, e_loc, weight, c_info, submission, deadline)
    
    def test_init_failsToSetProperties_weightNegative(self):

        #Arrange and act
        s_loc, e_loc, weight, c_info = 'Sydney', 'Melbourne', -10, 'pesho@gmail.com'
        submission  = DateCreator('0:00', '12.10.2022')
        deadline = DateCreator('23:59', '15.10.2022')
        
        #Assert
        with self.assertRaises(ValueError):
            Package(s_loc, e_loc, weight, c_info, submission, deadline)
    
    def test_init_failsToSetProperties_contactInfoInvalid(self):

        #Arrange and act
        s_loc, e_loc, weight, c_info = 'Sydney', 'Melbourne', 9000, 'joropetrov'
        submission  = DateCreator('0:00', '12.10.2022')
        deadline = DateCreator('23:59', '15.10.2022')
        
        #Assert
        with self.assertRaises(ValueError):
            Package(s_loc, e_loc, weight, c_info, submission, deadline)
    
    def test_init_failsToSetProperties_datesNotInOrder(self):

        #Arrange and act
        s_loc, e_loc, weight, c_info = 'Sydney', 'Melbourne', 9000, 'pesho@gmail.com'
        submission  = DateCreator('23:59', '15.10.2022')
        deadline = DateCreator('0:00', '12.10.2022')
        
        #Assert
        with self.assertRaises(ValueError):
            Package(s_loc, e_loc, weight, c_info, submission, deadline)
    
    def test_info_returnsStringCorrectly(self):

        #Arrange and act
        s_loc, e_loc, weight, c_info = 'Sydney', 'Melbourne', 9000, 'pesho@gmail.com'
        submission  = DateCreator('0:00', '12.10.2022')
        deadline = DateCreator('23:59', '15.10.2022')
        package = Package(s_loc, e_loc, weight, c_info, submission, deadline)
        
        stringified = [f'Package {package.id}:', f' from: {s_loc}, to: {e_loc}', f' weight: {weight}', f' sender: {c_info}', 
        f' sumbmitted: {submission} deadline: {deadline}']
        stringified = '\n'.join(stringified)

        #Assert
        self.assertEqual(package.info(), stringified)
    
    def test_str_returnsStringCorrectly(self):

        #Arrange and act
        s_loc, e_loc, weight, c_info = 'Sydney', 'Melbourne', 9000, 'pesho@gmail.com'
        submission  = DateCreator('0:00', '12.10.2022')
        deadline = DateCreator('23:59', '15.10.2022')
        package = Package(s_loc, e_loc, weight, c_info, submission, deadline)
        stringified = f'Package weight: {weight}kg'

        #Assert
        self.assertEqual(str(package), stringified)
        
