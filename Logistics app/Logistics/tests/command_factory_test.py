import unittest
from commands.add_truck_command import AddTruckCommand
from commands.add_package import AddPackage
from commands.create_package_command import CreatePackageCommand
from commands.create_route_command import CreateRouteCommand
from commands.find_suitable_routes import FindSuitableRoutes
from commands.view_package_command import ViewPackageCommand
from commands.view_route_command import ViewRouteCommand
from core.application_data import ApplicationData
from core.command_factory import CommandFactory


def factory_and_data():
    app_data = ApplicationData()
    factory = CommandFactory(app_data)

    return factory, app_data

class CommandFactory_Should(unittest.TestCase):
    def test_create_raisesError_invalidCommandName(self):
        # Arrange
        input_line = 'hello'
        factory, app_data = factory_and_data()

        # Act & Assert
        with self.assertRaises(ValueError):
            command = factory.create(input_line)
    
    def test_create_createsCreateRouteCommand_withCorrectParams(self):
        # Arrange
        input_line = 'createroute Sydney:Melbourne:Perth:Darwin 22:54 12.08.2022'
        factory, app_data = factory_and_data()

        # Act
        command = factory.create(input_line)

        # Assert
        self.assertIsInstance(command, CreateRouteCommand)
        self.assertEqual(app_data, command.app_data)
        self.assertEqual(tuple(['Sydney:Melbourne:Perth:Darwin', '22:54', '12.08.2022']), command.params)
    
    def test_create_createsViewRouteCommand_withCorrectParams(self):
        # Arrange
        input_line = 'ViewRoute 1'
        factory, app_data = factory_and_data()

        # Act
        command = factory.create(input_line)

        # Assert
        self.assertIsInstance(command, ViewRouteCommand)
        self.assertEqual(app_data, command.app_data)
        self.assertEqual(tuple(['1']), command.params)
    
    def test_create_createsAddTruckCommand_withCorrectParams(self):
        # Arrange
        input_line = 'addtruck 1 1035'
        factory, app_data = factory_and_data()

        # Act
        command = factory.create(input_line)

        # Assert
        self.assertIsInstance(command, AddTruckCommand)
        self.assertEqual(app_data, command.app_data)
        self.assertEqual(tuple(['1','1035']), command.params)
    
    def test_create_createsAddPackage_withCorrectParams(self):
        # Arrange
        input_line = 'AddPackage 1 2'
        factory, app_data = factory_and_data()

        # Act
        command = factory.create(input_line)

        # Assert
        self.assertIsInstance(command, AddPackage)
        self.assertEqual(app_data, command.app_data)
        self.assertEqual(tuple(['1','2']), command.params)
    
    def test_create_createsCreatePackageCommand_withCorrectParams(self):
        # Arrange
        input_line = 'CreatePackage Adelaide Brisbane 1000 stamen@gmail.com 24.07.2022 29.07.2022'
        factory, app_data = factory_and_data()

        # Act
        command = factory.create(input_line)

        # Assert
        self.assertIsInstance(command, CreatePackageCommand)
        self.assertEqual(app_data, command.app_data)
        self.assertEqual(tuple(['Adelaide','Brisbane', '1000', 'stamen@gmail.com', '24.07.2022', '29.07.2022']), command.params)
    
    def test_create_createsViewPackageCommand_withCorrectParams(self):
        # Arrange
        input_line = 'ViewPackage 1'
        factory, app_data = factory_and_data()

        # Act
        command = factory.create(input_line)

        # Assert
        self.assertIsInstance(command, ViewPackageCommand)
        self.assertEqual(app_data, command.app_data)
        self.assertEqual(tuple(['1']), command.params)
    
    def test_create_createsFindSuitableRoutes_withCorrectParams(self):
        # Arrange
        input_line = 'FindSuitableRoutes 1'
        factory, app_data = factory_and_data()

        # Act
        command = factory.create(input_line)

        # Assert
        self.assertIsInstance(command, FindSuitableRoutes)
        self.assertEqual(app_data, command.app_data)
        self.assertEqual(tuple(['1']), command.params)
