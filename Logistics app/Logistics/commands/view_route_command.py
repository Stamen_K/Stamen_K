from commands.base_command import BaseCommand
from core.application_data import ApplicationData


class ViewRouteCommand(BaseCommand):
    def __init__(self, params: list[str], app_data: ApplicationData):
        super().__init__(params, app_data)
    
    def execute(self):
        routeID = int(self.params[0])
        
        for route in self.app_data.routes:
            if route.id == routeID:
                return str(route)