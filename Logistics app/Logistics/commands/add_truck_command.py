from commands.base_command import BaseCommand
from core.application_data import ApplicationData
from models.truck import Truck


class AddTruckCommand(BaseCommand):
    def __init__(self, params: list[str], app_data: ApplicationData):
        super().__init__(params, app_data)
    
    def execute(self):
        routeID = int(self.params[0])
        truckID = int(self.params[1])
        
        if self.app_data.check_vehicle(truckID) == False:
            raise ValueError('Truck is unavailable or non existant')
        
        for truck in self.app_data.available:
            if truck.id == truckID:
                for route in self._app_data.routes:
                    if route.id == routeID:
                        if truck.max_range >= route.total_distance:
                            route.add_truck(truck)
                            self._app_data.remove_vehicle(truckID)
                            break
                        else:
                            raise ValueError('Truck range too little')
        
        return f'Truck {truckID} succesfully added to Route {routeID}'
