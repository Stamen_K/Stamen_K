from commands.base_command import BaseCommand
from core.application_data import ApplicationData
from models.date_creator import DateCreator
from models.package import Package


class CreatePackageCommand(BaseCommand):
    def __init__(self, params: list[str], app_data: ApplicationData):
        super().__init__(params, app_data)
    
    def execute(self):
        default_submit = '0:00'
        default_deadline = '23:59'
        start_loc = self.params[0]
        end_loc = self.params[1]
        weight = int(self.params[2])
        contact_info = self.params[3]
        submission = DateCreator(default_submit, self.params[4])
        deadline = DateCreator(default_deadline, self.params[5])

        package = Package(start_loc, end_loc, weight, contact_info, submission, deadline)
        self.app_data.create_package(package)

        return f'Package {package.id} created'
