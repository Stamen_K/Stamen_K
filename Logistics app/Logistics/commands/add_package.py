from commands.base_command import BaseCommand
from core.application_data import ApplicationData



class AddPackage(BaseCommand):
    def __init__(self, params: list[str], app_data: ApplicationData):
        super().__init__(params, app_data)
    
    def execute(self):
        packageID = int(self.params[0])
        routeID = int(self.params[1])
        
        try:
            self.app_data.add_package(packageID, routeID)
        except ValueError:
            return 'Package can not be added to route'
        return f'Package {packageID} added to route {routeID}'
    


        
      
                