from commands.base_command import BaseCommand
from core.application_data import ApplicationData
from models.date_creator import DateCreator
from models.deliver_route import Route
from models.truck import Truck


class CreateRouteCommand(BaseCommand):
    def __init__(self, params: list[str], app_data: ApplicationData):
        super().__init__(params, app_data)
    
    def execute(self):
        locations = self.params[0].split(':')
        str_time = self.params[1]
        str_dates = self.params[2]

        route = Route(locations, str_time, str_dates)
        self._app_data.add_route(route)

        return f'Added route {route.id}'
