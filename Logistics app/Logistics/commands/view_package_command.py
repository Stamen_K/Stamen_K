from commands.base_command import BaseCommand
from core.application_data import ApplicationData


class ViewPackageCommand(BaseCommand):
    def __init__(self, params: list[str], app_data: ApplicationData):
        super().__init__(params, app_data)
    
    def execute(self):
        packageID = int(self.params[0])

        for package in self.app_data.packages:
            if package.id == packageID:
                for route in self.app_data.routes:
                    for k in range(len(route._locations)-1):
                        if route._locations[k] == package.end_loc:
                            for element in route.truck.trunk:
                                if element.id == packageID:
                                    arr_time = str(route.dates[k])
                                    return f'{package.info()}, Expected arrival time: {arr_time}'
                return package.info() 
        raise ValueError('No such package')
