from commands.base_command import BaseCommand
from core.application_data import ApplicationData
from validation_helpers.compare_dates import  isbigger_date


class FindSuitableRoutes(BaseCommand):
    def __init__(self, params: list[str], app_data: ApplicationData):
        super().__init__(params, app_data)
    
    def execute(self):
        packageID = int(self.params[0])

        output = ['Suitable Routes:']

        for element in self.app_data.packages:
            if element.id == packageID:
                package = element
        
        for route in self._app_data.routes:
            if route.truck.capacity < package.weight:
                continue
            if package.start_loc and package.end_loc in route.locations:
                if route.locations.index(package.start_loc) < route.locations.index(package.end_loc) and isbigger_date(route.dates[route.locations.index(package.start_loc)], package.submission) and isbigger_date(package.deadline, route.dates[route.locations.index(package.end_loc)]):
                    output.append(route.info())
        
        if len(output) == 1:
            return 'No suitable route found'
        output = '\n'.join(output)
        return output